#!/bin/sh
PTNlogin='^[a-zA-Z]*$'
##Passage en keymap us pour le password comme sa quelque soit votre clavier le mot de passe correspond bien ##au touche frappée.ce qui, ce qui évite de ce prendre la tête pour le clavier avec le mot de passe grub.
#apt-get install console-data
layout1="$(cat < /etc/default/keyboard | grep "XKBLAYOUT" | awk -F "\"" '{print $2}' | awk -F "," '{print $1}')"
setxkbmap us
loadkeys us
clear
gettext 'keymap is in qwerty us in grub menu.
    - Only letters or numbers.
    - 4 characters minimum.
Enter login to the superuser of grub2 :'
while (true); do
    read logingrub
    if [ "$(expr "$logingrub" : "$PTNlogin")" -gt 4  ];then 
        break
    else
        clear
gettext 'keymap is in qwerty us in grub menu.
    - Only letters or numbers.
    - 4 characters minimum.
Enter login to the superuser of grub2 :'
    fi    
done
echo > /tmp/passgrub
while [ "$(awk '{print $NF}' /tmp/passgrub | grep -c grub.)" -eq 0 ];do
grub-mkpasswd-pbkdf2 | tee /tmp/passgrub
done    
passwordgrub=$(awk '{print $NF}' /tmp/passgrub | grep grub.)
##on rebascule sur la keymap system
setxkbmap "$layout1"
loadkeys "$layout1"
vide=""
cat << EOF > /etc/grub.d/99_password
#!/bin/sh
## ce script doit être lancé en dernier !!!
## on restreint uniquement les menus de "setup uefi" , " recovery mode "
## ainsi que tous les submenu.
## seul les menuentry et submenu de premier niveaux prennent en compte les paramètres d’accès utilisateurs.
## ce qui implique que l'ajout de --unrestricted a un submenu et récursif  !!
cat << ${vide}EOF
set superusers="$logingrub"
password_pbkdf2 $logingrub $passwordgrub
${vide}EOF
confunrestricted() {
        ## fonction lancer en tache de fond par la commande
        ## confunrestricted &
        ## elle attend la fin de l’exécution de tous les scripts  update grub 
        ## puis modifie le fichier /boot/grub/grub.cfg pour y ajouter le droits a touts le monde
        cd /etc/grub.d/
        for file in *
        do
        if [ "\$(echo "\$file" | grep -E -c "[0-9][0-9]_")" -eq 1 ];then
            if [ -z "\$processupdategrub" ] ; then
                processupdategrub=\$file
            else
                processupdategrub=\$processupdategrub","\$file
            fi
        fi
        done
        while [ "\$(ps -C "\$processupdategrub" -o pid= | wc -l)" -gt 2 ]
        do
            sleep 0.2
        done
        cp /boot/grub/grub.cfg /tmp/grub.cfg.new
        while read linecfg 
        do
            if [ "\$(echo "\$linecfg" | grep -E "menuentry " | grep -v "uefi-firmware" | grep -c -v "recovery mode" )" -eq 1 ];then
                line2=\$(echo "\$linecfg" | sed -e 's/ {/ --unrestricted { /g')
                sed -i "s|\$linecfg|\$line2|" /boot/grub/grub.cfg
            fi
        done < /tmp/grub.cfg.new
        rm /tmp/grub.cfg.new
    }
confunrestricted &
EOF
chmod 755 /etc/grub.d/99_password
update-grub2
