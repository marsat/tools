#NoTrayIcon
#RequireAdmin
#include <Misc.au3>
$versionfusioninventory="2.4"
$ipdeploiment="0006" ; incrémenter ce chifre a chaque verssion ou changement de configurations
if @OSArch = "X86" Then
   $filtreengine="x86"
   $installdir="C:\Program Files\FusionInventory-Agent\"
   $fusioninventoryagentexe="fusioninventory-agent_windows-x86_"&$versionfusioninventory&".exe"
ElseIf @OSArch = "X64" Then
   $filtreengine="x64"
   $installdir="C:\Program Files\FusionInventory-Agent\"
   $fusioninventoryagentexe="fusioninventory-agent_windows-x64_"&$versionfusioninventory&".exe"
ElseIf @OSArch = "IA64" Then
   $filtreengine="ia64"
   $installdir="C:\Program Files\FusionInventory-Agent\"
   $fusioninventoryagentexe="fusioninventory-agent_windows-x64_"&$versionfusioninventory&".exe"
Else
   msgbox ( 48,"Erreur","Architecture non supporter",5)
   ConsoleWrite ("Architecture non supporter" &@CRLF)
   Exit 2
EndIf

$fileconf=StringSplit(@ScriptName,".")[StringSplit(@ScriptName,".")[0]-1]&$filtreengine&".ini"
$tag=""
$tag=InputBox ( "Tag","Entrer le tag voulus pour le poste","")

$fusioninventoryparam="  /S /execmode=Service /delaytime=3600 /runnow /acceptlicense /ca-cert-file="&chr(34)&$installdir&"ca.glpi.pem"&chr(34)&" /server="&chr(34)&"https://glpi.mydomain/plugins/fusioninventory/"&chr(34)&" /add-firewall-exception /tag="&chr(34)&$tag&chr(34)
$extention=StringSplit(@ScriptName,".")[StringSplit(@ScriptName,".")[0]]
;~ if FileExists ($fileconf) Then

;~ $fusioninventoryagentexe = IniRead ( $fileconf , "CONFIG", "fusioninventoryagentexe", $fusioninventoryagentexe )
;~ $fusioninventoryparam =IniRead ( $fileconf , "CONFIG", "fusioninventoryparam", $fusioninventoryparam)
;~ $ipdeploiment = IniRead ( $fileconf , "CONFIG", "ipdeploiment", $ipdeploiment)
;~ Else
;~    IniWrite($fileconf , "CONFIG", "fusioninventoryagentexe", $fusioninventoryagentexe)
;~    IniWrite($fileconf , "CONFIG", "fusioninventoryparam", $fusioninventoryparam )
;~    IniWrite($fileconf , "CONFIG", "ipdeploiment", $ipdeploiment )
;~ EndIf

$IDfile=$installdir&"ID_gpo_install.ini"
;~ ConsoleWrite ($IDfile)

if FileExists ($IDfile) Then
$idinstall = IniRead ( $IDfile , "CONFIG", "id", $ipdeploiment )
ConsoleWrite ($idinstall &"  "& $ipdeploiment )
   if StringCompare ( $idinstall , $ipdeploiment ) < 0 Then
	  ConsoleWrite( "install1")
	  ConsoleWrite (@ScriptDir&"\"&$fusioninventoryagentexe&" "&$fusioninventoryparam)
	  DirCreate ( $installdir )
	  FileCopy (@scriptdir&"\ca.glpi.pem" , $installdir,1)
	  RunWait (@ScriptDir&"\"&$fusioninventoryagentexe&" "&$fusioninventoryparam)
	  RunWait ($installdir&"\fusioninventory-agent.bat",@SystemDir,@SW_HIDE)
	  IniWrite($IDfile , "CONFIG", "id", $ipdeploiment)

   EndIf
Else
   ConsoleWrite( "install2")
   DirCreate ( $installdir )
   FileCopy (@scriptdir&"\ca.glpi.pem" , $installdir,1)
   ConsoleWrite (@ScriptDir&"\"&$fusioninventoryagentexe&" "&$fusioninventoryparam)
   RunWait (@ScriptDir&"\"&$fusioninventoryagentexe&" "&$fusioninventoryparam)
   RunWait ($installdir&"\fusioninventory-agent.bat",@SystemDir,@SW_HIDE)
   IniWrite($IDfile , "CONFIG", "id", $ipdeploiment)
EndIf
