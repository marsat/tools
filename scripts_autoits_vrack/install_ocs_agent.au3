#NoTrayIcon
#RequireAdmin
#include <Misc.au3>

if @OSArch = "X86" Then
   $filtreengine=""
   $PROGFILE="C:\Program Files"
ElseIf @OSArch = "X64" Then
   $filtreengine="amd64"
   $PROGFILE="C:\Program Files (x86)"
ElseIf @OSArch = "IA64" Then
   $filtreengine="ia64"
   $PROGFILE="C:\Program Files (x86)"
Else
   msgbox ( 48,"Erreur","Architecture non supporter",5)
   ConsoleWrite ("Architecture non supporter" &@CRLF)
   Exit 2
EndIf

$fileconf=StringSplit(@ScriptName,".")[StringSplit(@ScriptName,".")[0]-1]&".ini"
$ipdeploiment="0001" ; incrémenter ce chifre a chaque verssion
$ocsagentexe="OCS-NG-Windows-Agent-Setup.exe"
$ocsparam="  /S /UPGRADE /NOTAG=1 /NOSPLASH /NO_SYSTRAY /DEBUG=1 /SERVER=http://srvocs/ocsinventory /SSL=0"
$extention=StringSplit(@ScriptName,".")[StringSplit(@ScriptName,".")[0]]
if FileExists ($fileconf) Then

$ocsagentexe = IniRead ( $fileconf , "CONFIG", "ocsagentexe", $ocsagentexe )
$ocsparam =IniRead ( $fileconf , "CONFIG", "ocsparam", $ocsparam)
$ipdeploiment = IniRead ( $fileconf , "CONFIG", "ipdeploiment", $ipdeploiment)
Else
   IniWrite($fileconf , "CONFIG", "ocsagentexe", $ocsagentexe)
   IniWrite($fileconf , "CONFIG", "ocsparam", $ocsparam )
   IniWrite($fileconf , "CONFIG", "ipdeploiment", $ipdeploiment )
EndIf
$installdir=$PROGFILE&"\OCS Inventory Agent\"

$IDfile=$installdir&"ID_gpo_install.ini"

$installdir=@ScriptDir&"\"
if FileExists ($IDfile) Then
$idinstall = IniRead ( $IDfile , "CONFIG", "id", $ipdeploiment )
ConsoleWrite ($idinstall &"  "& $ipdeploiment )
   if StringCompare ( $idinstall , $ipdeploiment ) < 0 Then
	  ConsoleWrite( "install1")
	  ConsoleWrite (@ScriptDir&"\"&$ocsagentexe&" "&$ocsparam)
	  RunWait (@ScriptDir&"\"&$ocsagentexe&" "&$ocsparam)
	  IniWrite($IDfile , "CONFIG", "id", $ipdeploiment)
	  FileCopy (@scriptdir&"\cacert.pem" ,@AppDataCommonDir &"\OCS Inventory NG\Agent\",1)
   EndIf
Else
   ConsoleWrite( "install2")
   IniWrite($IDfile , "CONFIG", "id", $ipdeploiment)
   ConsoleWrite (@ScriptDir&"\"&$ocsagentexe&" "&$ocsparam)
   RunWait (@ScriptDir&"\"&$ocsagentexe&" "&$ocsparam)
   FileCopy (@scriptdir&"\cacert.pem" ,@AppDataCommonDir &"\OCS Inventory NG\Agent\",1)
EndIf
