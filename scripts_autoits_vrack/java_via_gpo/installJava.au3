#NoTrayIcon



;~ $uidInstalledVersion="{26A24AE4-039D-4CA4-87B4-2F83218066F0}"
$RepJava="C:\Program Files (x86)\Java"
$RepJavaSizeFinal=144275449
$version="8u73"
$uidInstalledVersion="{26A24AE4-039D-4CA4-87B4-2F8321"&StringReplace ($version,"u","0")&"F0}"
$JavaExe="jre-"&$version&"-windows-i586.exe"
if not FileExists (@ScriptDir&"\"&$JavaExe) Then
   Exit 10
EndIf

$ParamJavaExe=" INSTALL_SILENT=1 STATIC=0 AUTO_UPDATE=0 WEB_JAVA=1 WEB_JAVA_SECURITY_LEVEL=H WEB_ANALYTICS=0 EULA=0 REBOOT=1 SPONSORS=0"

;On vérifi si cette version de java est deja installer si telle est le cas on sort du programe.
$i=1
While true
	$var = RegEnumKey("HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\", $i)
	If @error <> 0 then ExitLoop ; sort de la boucle si plus de clef a lire.
;~    ;ConsoleWrite ($var & @CRLF)
	if ( StringCompare ( $var , $uidInstalledVersion ) = 0 ) Then
	   ConsoleWrite ( "java "&$uidInstalledVersion & "est deja instalé.")
	   Exit
    EndIf
	$i=1+$i
WEnd

$installok=0
$time=300
ProgressOn("Mise a jour JAVA "&$version&" en cours", "Fermeture des Navigateurs dans:", $time & " secondes" , Default , Default , 16 )
	For $i = 1 to $time step 1
		sleep(1000)
		ProgressSet( $i*100/$time , $time-$i & " secondes")
		if  not ProcessExists ("firefox.exe") Then
			If not ProcessExists ("iexplore.exe") then
				ExitLoop
			EndIf
		EndIf
	Next
ProgressOff()
ProgressOn("Mise a jour JAVA "&$version&" en cours", "ne rien toucher et Patienter" , "" , Default , Default , 16 )

RunWait ( "taskkill /f /im iexplore.exe","", @SW_HIDE)
RunWait ( "taskkill /f /im firefox.exe","", @SW_HIDE)
RunWait ( "taskkill /f /im jqs.exe","", @SW_HIDE)
RunWait ( "taskkill /f /im jusched.exe","", @SW_HIDE)




; récupération des commandes pour désintaller anciennes versions de java
$i=1
$y=1
dim $uninstall[2]
While true
	$var = RegEnumKey("HKLM64\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\", $i)
	If @error <> 0 then ExitLoop ; sort de la boucle si plus de clef a lire.
;~    ConsoleWrite ($var & @CRLF)
	if ( StringInStr ($var , "{26A24AE4-039D-4CA4-87B4-2F" ) And not (StringCompare ( $var , $uidInstalledVersion ) = 0 )) Then
	   ConsoleWrite ($var & @CRLF)
		$uninstall[$y]="MsiExec.exe /passive /X"&$var
		$y=1+$y
	EndIf
	if ( StringInStr ($var , "{3248F0A8-6813-11D6-A77B-00B0D0" ) And not (StringCompare ( $var , $uidInstalledVersion ) = 0 )) Then
		$uninstall[$y]="MsiExec.exe /passive /X"&$var
		$y=1+$y

	EndIf
	if (StringCompare ( $var ,$uidInstalledVersion ) = 0 ) Then
		$installok=1
	EndIf
	$i=1+$i
	ReDim $uninstall[1+$y]
WEnd
 ; récupération des commandes pour désintaller anciennes versions de java (application 32bit sous windows 7)
$i=1
While true
	$var2 = RegEnumKey("HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\", $i)
	If @error <> 0 then ExitLoop ; sort de la boucle si plus de clef a lire.
;~ 	    ConsoleWrite ($var & @CRLF)
	if ( StringInStr ($var2 , "{26A24AE4-039D-4CA4-87B4-2F" ) And not (StringCompare ( $var2 ,$uidInstalledVersion ) = 0 )) Then
;~ 	    ConsoleWrite ($var2 & @CRLF)
		$uninstall[$y]="MsiExec.exe /passive /X"&$var2
		$y=1+$y
	EndIf
	if ( StringInStr ($var2 , "{3248F0A8-6813-11D6-A77B-00B0D0" ) And not (StringCompare ( $var2 ,$uidInstalledVersion ) = 0 )) Then
		$uninstall[$y]="MsiExec.exe /passive /X"&$var2
		$y=1+$y
	EndIf
	if (StringCompare ( $var2 ,$uidInstalledVersion ) = 0 ) Then
		$installok=1
	EndIf
	$i=1+$i
	ReDim $uninstall[1+$y]
 WEnd
 $i=1
While true
	$var2 = RegEnumKey("HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\", $i)
	If @error <> 0 then ExitLoop ; sort de la boucle si plus de clef a lire.
;~ 	    ConsoleWrite ($var & @CRLF)

	if ( StringInStr ($var2 , "{26A24AE4-039D-4CA4-87B4-2F" ) And not (StringCompare ( $var2 ,$uidInstalledVersion ) = 0 )) Then
	    ConsoleWrite ($var2 & @CRLF)
		$uninstall[$y]="MsiExec.exe /passive /X"&$var2
		$y=1+$y
	EndIf
	if ( StringInStr ($var2 , "{3248F0A8-6813-11D6-A77B-00B0D0" ) And not (StringCompare ( $var2 ,$uidInstalledVersion ) = 0 )) Then
		$uninstall[$y]="MsiExec.exe /passive /X"&$var2
		$y=1+$y
	EndIf
	if (StringCompare ( $var2 ,$uidInstalledVersion ) = 0 ) Then
		$installok=1
	EndIf
	$i=1+$i
	ReDim $uninstall[1+$y]
 WEnd


 ;_______________________________________________________________________


 ; supression des anciennes versions de java
 ProgressSet( 1 , "Suppression des anciennes versions de java")
 sleep( 1000 )
$RepJavaSizeOrig=DirGetSize ( $RepJava  )
For $i=1 to UBound($uninstall) - 2
	If StringInStr ($uninstall[$i], "MsiExec.exe") Then
		If StringInStr ($uninstall[$i] , "{3248F0A8-6813-11D6-A77B-00B0D0" ) then
			$Vp=StringMid($uninstall[$i], 54, 1)
			$Vu=StringMid($uninstall[$i], 56, 2)
		Else
			$Vp=StringMid($uninstall[$i], 53, 1)
			$Vu=StringMid($uninstall[$i], 55, 2)
		EndIf
		ProgressSet( 1 , "Suppression de java "&$Vp&"u"&$Vu)
		ConsoleWrite ($uninstall[$i])
		$pid=Run( $uninstall[$i] )
		If @error  <> 0 then Exit @error
		ConsoleWrite ($pid)
		sleep ( 2000 )
		while ProcessExists ( $pid )
			$RepJavaSizeActuel=DirGetSize ( $RepJava )
			ProgressSet( (($RepJavaSizeOrig - $RepJavaSizeActuel)/$RepJavaSizeOrig)*100  )
			sleep ( 1000 )
		 WEnd
		 ConsoleWrite ("Suppression de java "&$Vp&"u"&$Vu&@crlf)
		sleep ( 1000 )
	EndIf
 Next




DirRemove ( $RepJava , 1 )
ProgressSet( 100 , "Suppression des anciennes versions de java OK")
sleep (1000)
 ;_______________________________________________________________________



 ; install nouvelle version
RunWait ( "taskkill /f /im iexplore.exe","", @SW_HIDE)
RunWait ( "taskkill /f /im firefox.exe","", @SW_HIDE)
RunWait ( "taskkill /f /im jqs.exe","", @SW_HIDE)
RunWait ( "taskkill /f /im jusched.exe","", @SW_HIDE)
RunWait ( "taskkill /f /im MsiExec.exe","", @SW_HIDE)

ConsoleWrite (@ComSpec & " /c "&chr(34)& @ScriptDir &"\"&$JavaExe&chr(34)&" "&$ParamJavaExe & @CRLF)

$pid=Run(@ComSpec & " /c "&chr(34)& @ScriptDir &"\"&$JavaExe&chr(34)&" "&$ParamJavaExe ,"",@SW_HIDE   )
If @error <> 0 then    Exit @error
    ConsoleWrite ($pid)
    ProgressSet( 0,"Installation de java "&$version&" OK" )

	sleep ( 500 )
	while ProcessExists ( $pid )
	  While not FileExists ($RepJava)
		  sleep ( 1000 )
	  WEnd
		$RepJavaSizeActuel = DirGetSize ( $RepJava )
		ProgressSet( ($RepJavaSizeActuel/$RepJavaSizeFinal )*95  )
		sleep ( 1000 )
	WEnd
	ConsoleWrite ( " ok"&@CRLF )


 sleep (2000)

; désinstall java auto update
$pid=Run( "MsiExec.exe /passive /X {4A03706F-666A-4037-7777-5F2748764D10}" )
		If @error  <> 0 then Exit @error
		sleep ( 500 )
		while ProcessExists ( $pid )
			sleep ( 1000 )
		 WEnd
ProgressSet( 100 , "Installation de java "&$version&" OK")
ProgressOff()
ConsoleWrite ( " ok"&@CRLF )
Sleep ( 20000 ); on laise le temps a l'install de ce finir (au cas ou).
Exit @error



