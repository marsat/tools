#NoTrayIcon
#include <File.au3>
#include <Constants.au3>
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.10.2
 Author:         Guillaume MARSAT

 Script Function:

	Client de prise en mains des pc/serveurs sur site distant via internet et tunnel ssh
	avec autentification par clef priv�e, clef public avec ou sans passephrase.

	Une foie compil� cr�er un racoursi windows en y ajoutent les param�tres pour joindres le serveur distants.
	cela permet une administration par simple clic sur le racourci par la suite avec juste la passphrase a rentr� si votre clef priv�
	en poss�de une.

	exemple1:
	d:\alcasar_dist.exe -I xxx.xxx.xxx.xxx -P yyyyy -V 192.168.182.65 -pk g:\ALCASAR_dist\alcasar-admin.ppk
    xxx.xxx.xxx.xxx=ip public du site distant
    yyyyy=port nater vers le serveur ssh distant.
    192.168.182.65 IP du pc a prendre a distance.
	g:\ALCASAR_dist\alcasar-admin.ppk clef privers avec ou sans passphrase autoriser a acc�der aux serveur ssh distant.


 Pr�requit:
	PuTTY (plinke.exe) doit �tre installer.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
Global $ipAddress
Global $ssh_pid
Global $hostname
Global $loginssh
Global $Pvnc
Global $Pssh
Global $ClePrivet
global $help = false
global $timeadditionel = 0
Global $cmdwin
if @OSArch = "X64" Then
   $progfile='C:\Program Files (x86)'
Else
   $progfile='C:\Program Files'
EndIf
$RepPlinke = $progfile&'\PuTTY'
$ClePrivet= 'D:\ALCASAR_dist\alcasardist.ppk'; rentrais ici le chemin ver le fichier de clef privet ne doit pas contenir d'espace!! Programe File et a remplacer par exemple par Progra~1
$PTdistant = '3389' ;  port d'�coute https par Default
$Pssh = '60230 ' ; port d'�coute copssh par Default
$vhost = ''
$loginssh='sysadmin'
$PTlocal = findportdisp()
if $CmdLine[0] <> 0 then
			$Nparm = $CmdLine[0]
			For $i = 1 To $Nparm
			   if $CmdLine[$i] = "-PD" Then
					 $PTdistant	 = $CmdLine[$i+1]
			   EndIf
			   if $CmdLine[$i] = "-V" Then
					 $vhost = $CmdLine[$i+1]
			   EndIf
			   if $CmdLine[$i] = "-I" Then
					 $ipAddress = $CmdLine[$i+1]
			   EndIf
			   if $CmdLine[$i] = "-P" Then
					 $Pssh = $CmdLine[$i+1]
			   EndIf
			   if $CmdLine[$i] = "-PL" Then
					 $PTlocal = $CmdLine[$i+1]
			   EndIf
			   if $CmdLine[$i] = "-L" Then
					 $loginssh = $CmdLine[$i+1]
			   EndIf
			   if $CmdLine[$i] = "-pk" Then
					 $ClePrivet = $CmdLine[$i+1]
			   EndIf
			   if $CmdLine[$i] = "-help" Then
					 $help = True
			   EndIf
			   if $CmdLine[$i] = "/?" Then
					 $help = True
			   EndIf
			Next
EndIf

If $help = true Then
        MsgBox(0, "Help", "Syntaxe "&@ScriptName&" [Options1 Options2 ... Optionsn ]" & @CRLF & _
		"Options" & @CRLF &  _
		"-I xxx.xxx.xxx.xxx " & @CRLF & _
		"	IP public serveur distant" & @CRLF & _
		"-P" & @CRLF & _
		"	Port public d'�coute du serveur distant (par default 60230)" & @CRLF & _
		"-PL" & @CRLF & _
		"	Port local du tunnel ssh (d�fault premier port libre entre 1023 et 2048)" & @CRLF & _
		"-PD" & @CRLF & _
		"	Port local d'�coute du serveur distant (par default 3389)" & @CRLF & _
		"-L "& @CRLF & _
		"	login du compte ssh (par default sysadmin)" & @CRLF & _
		"-pk " & @CRLF & _
		"	Chemin ver le fichier de clef privet file.ppk"& @CRLF & _
		"	ne doit pas contenir d'espace! (D:\ALCASAR_dist\alcasardist.ppk par default)" & @CRLF & _
		"-V "& @CRLF & _
		"	Nom dns ou IP du serveurs distants a joindre"& @CRLF & _
		"-help ou /? retourne l'aide"& @CRLF & _
		"Exemple:"	& @CRLF & _
		@ScriptName&" -I xxx.xxx.xxx.xxx -P yyyyy -V 192.168.182.65 -pk g:\ALCASAR_dist\alcasar-admin.ppk")
		Exit
EndIf
If FileExists($RepPlinke & '\plink.exe') = 0 Then
        MsgBox(0, "ERROR", 'Veuiller installer plink.exe (Putty) dans ' & $RepPlinke )
        Exit
EndIf

If FileExists($ClePrivet)= 0 then
        MsgBox(0, "ERROR", 'Veuiller installer la clef privet '&$ClePrivet)
        Exit
EndIf
If  Ping ( "www.google.fr") = 0 Then
        MsgBox(0, "ERROR", 'Veuiller vous connecter a internet.')
        Exit
EndIf

if $ipAddress = '' Then
   $ipAddress= InputBox( "adresse IP", "Entrer l'@ IP du serveur alcasar distant", "", "")
   If $ipAddress = '' Then
	  Exit
   EndIf
EndIf
if $vhost = '' Then
   $vhost= InputBox( "adresse CLIENT", "Entrer le NOM/IP du PC distant", "", "")
   If $vhost = '' Then
	  Exit
   EndIf
EndIf
If StringLower(@ComputerName) = StringLower($ipAddress) Then
   MsgBox(0, "ERROR", "On ne peut ouvrir un tunnel ssh sur localhost !!!" )
   Exit
Endif
If StringLower(@IPAddress1) = StringLower($ipAddress) Then
   MsgBox(0, "ERROR", "On ne peut ouvrir un tunnel ssh sur localhost !!!")
   Exit
Endif
If StringLower(@IPAddress2) = StringLower($ipAddress) Then
   MsgBox(0, "ERROR", "On ne peut ouvrir un tunnel ssh sur localhost !!! ")
   Exit
Endif
If $loginssh = '' Then
   $loginssh = InputBox("S�curit�", "Entrez login du Tunnel ssh", "sysadmin" ,"")
EndIf
OpenTunnelSSH()
$exitlooptime=60*4
$time=0
While tunnelisopen ($PTlocal)=False
   sleep ( 250 )
   $time=$time+1
   if $time>$exitlooptime Then
	  EndTunnelSsh()
	  MsgBox ( 0, "Message","temps de connection au serveur d�passer.")
	  Exit
   EndIf
WEnd
WinSetState ($cmdwin,"",@SW_MINIMIZE)
$pidplink=Run (chr(34)& $RepPlinke & '\putty.exe'&chr(34) )
MsgBox ( 0, "","infos connection ssh:"&@crlf&"ip=127.0.0.1  port="&$PTlocal&@crlf&"clef ssh="&$ClePrivet)
While ProcessExists ($pidplink)
   sleep ( 1000 )
WEnd
EndTunnelSsh()

Exit


; #FUNCTION; ================================================================================
;
; Name..........:  EndTunnelSsh()

Func EndTunnelSsh()
   StdinWrite($ssh_pid, 'exit' & ' ' & @CR)
   Sleep(250)
   While WinExists ($cmdwin)
		 WinClose ($cmdwin)
   Wend
   ProcessClose ($ssh_pid)
EndFunc

; #FUNCTION; ================================================================================
;
; Name..........:  OpenTunnelSSH()
Func OpenTunnelSSH()
   $ssh_pid=Run('cmd.exe /k '&chr(34)& $RepPlinke & '\plink.exe'&chr(34)&' -ssh -P '& $Pssh &' -i '& $ClePrivet &' -L '& $PTlocal &':'& $vhost &':'& $PTdistant &' ' & $loginssh & '@' & $ipAddress )
   Sleep ( 250 )
   $cmdwin=WinGetHandle("[ACTIVE]")
   ConsoleWrite('cmd.exe /k '&chr(34)& $RepPlinke & '\plink.exe'&chr(34)&' -ssh -P '& $Pssh &' -i '& $ClePrivet &' -L '& $PTlocal &':'& $vhost &':'& $PTdistant &' ' & $loginssh & '@' & $ipAddress & @crlf )
EndFunc

; #FUNCTION; ================================================================================
;
; Name..........:  tunnelisopen()
Func tunnelisopen($port)
   $exe="C:\Windows\System32\NETSTAT.EXE"
   $param=" -a -n "
   $return=False
	ConsoleWrite (@ComSpec&" /c  "&chr(34)& $exe &chr(34) &$param & @CRLF)
	if @OSArch = "X64" Then
		 DllCall("kernel32.dll", "int", "Wow64DisableWow64FsRedirection", "int", 1)
    EndIf
	$pidcmd = run  ( @ComSpec&" /c  "&chr(34)& $exe &chr(34) &$param  ,@ScriptDir, @SW_HIDE , 0x2)
	local $stdoutcmd =""
	While 1
	  sleep ( 10 )
	  $tmp=StdoutRead ($pidcmd)
	  if StringCompare ( $tmp,"") =0 then
		 ConsoleWrite ( $tmp)
	  EndIf
	  $stdoutcmd = $stdoutcmd & $tmp
		if @error then
			ExitLoop
		 EndIf
		 if Not ProcessExists ( $pidcmd ) Then
			ExitLoop
		 EndIf
	WEnd
	$splitstdoutcmd=StringSplit ($stdoutcmd,@CRLF)
If $splitstdoutcmd[0]<>0 Then
   For $i=1 To $splitstdoutcmd[0]
	  if StringInStr ( $splitstdoutcmd[$i] , "127.0.0.1:"&$port) > 0 Then
		 	  if StringInStr ( $splitstdoutcmd[$i] , "LISTENING") > 0 Then
				  ProcessClose ( $pidcmd )
				  $return=True
			   EndIf
	  EndIf
   Next
EndIf
if @OSArch = "X64" Then
	  DllCall("kernel32.dll", "int", "Wow64DisableWow64FsRedirection", "int", 0)
EndIf
Return $return
EndFunc

; #FUNCTION; ================================================================================
;
; Name..........:  findportdisp()
func findportdisp ($portmin=1023,$portmax=2048)
   For $i=$portmin To $portmax
	  if Not socketisused ("127.0.0.1:"&$i) then
		 Return $i
	  EndIf
   Next
EndFunc

; #FUNCTION; ================================================================================
;
; Name..........:  socketisused()
Func socketisused($socket)
   $exe="C:\Windows\System32\NETSTAT.EXE"
   $param=" -a -n "
   $return=False
	ConsoleWrite (@ComSpec&" /c  "&chr(34)& $exe &chr(34) &$param & @CRLF)
	if @OSArch = "X64" Then
		 DllCall("kernel32.dll", "int", "Wow64DisableWow64FsRedirection", "int", 1)
    EndIf
	$pidcmd = run  ( @ComSpec&" /c  "&chr(34)& $exe &chr(34) &$param  ,@ScriptDir, @SW_HIDE , 0x2)
	local $stdoutcmd =""
	While 1
	  sleep ( 10 )
	  $tmp=StdoutRead ($pidcmd)
	  if StringCompare ( $tmp,"") =0 then
		 ConsoleWrite ( $tmp)
	  EndIf
	  $stdoutcmd = $stdoutcmd & $tmp
		if @error then
			ExitLoop
		 EndIf
		 if Not ProcessExists ( $pidcmd ) Then
			ExitLoop
		 EndIf


	WEnd
   $splitstdoutcmd=StringSplit ($stdoutcmd,@CRLF)
   If $splitstdoutcmd[0]<>0 Then
	  For $i=1 To $splitstdoutcmd[0]
		 if StringInStr ( $splitstdoutcmd[$i] , $socket) > 0 Then
			$return=True
		 EndIf
	  Next
   EndIf
   if @OSArch = "X64" Then
		 DllCall("kernel32.dll", "int", "Wow64DisableWow64FsRedirection", "int", 0)
   EndIf
   Return $return
EndFunc










