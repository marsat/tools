#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.10.2
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------
 #include <File.au3>
 #include <FileConstants.au3>
 #include <MsgBoxConstants.au3>
Veille(False)
 $Date=@MDAY&"/"&@MON&"/"&@YEAR
addlignetolog (@CRLF&"======================= "&$Date&" ======================="&@CRLF)

; Script Start - Add your code below here
;~ Name,SamAccountName,givenName,displayName,AccountPassword,Mail
;~ New-ADUser -ChangePasswordAtLogon $true -SamAccountName $_.SamAccountName -givenName $_.givenName -displayName $_.displayName -Name $_.Name -Enabled $True -Path 'OU=utilisateurs,OU=CGC,DC=internet,DC=local' -AccountPassword (Convertto-SecureString $_.AccountPassword  -AsPlainText -force) }
;~ NewADUser( "t.test","Azerty","pTest","Nom","OU=utilisateurs,OU=mydomain",True,False,"","","D�scrioption")
dim $ADalcasaruser
$filecsv=FileOpenDialog ( "CSV2AD", @ScriptDir , "All(*.csv)" ,0 )
 If @error Then
        ; Display the error message.
        MsgBox($MB_SYSTEMMODAL, "", "No file was selected.")
        ; Change the working directory (@WorkingDir) back to the location of the script directory as FileOpenDialog sets it to the last accessed folder.
        FileChangeDir(@ScriptDir)
		Exit
EndIf
_FileReadToArray ( $filecsv , $ADalcasaruser , 0 )


for  $line in $ADalcasaruser
   if $line="" Then
	  ExitLoop
   EndIf
   if StringInStr ( $line,"SamAccountName") = 0 Then
	  $infosuser=StringSplit ( $line , ",")
	  $Name=$infosuser[1]
	  ConsoleWrite ( @crlf & $Name )
	  $SamAccountName=$infosuser[2]
	  $givenName=$infosuser[3]
	  $displayName=$infosuser[4]
	  $AccountPassword=$infosuser[5]
	  NewADUser ($SamAccountName,$AccountPassword,$givenName,"","OU=standard,OU=utilisateurs,OU=mydomain",True,False,$Name,$displayName)
   EndIf
Next
Veille(True)
addlignetolog (@CRLF&"======================= End scripte ======================="&@CRLF)

Func NewADUser ($SamAccountName,$AccountPassword,$givenName,$sn="",$OU="CN=Users",$initpasswd=True,$AccountDisabled=False,$Name="",$displayName="",$Description="")
$ACCOUNTDISABLE=0x0002
$PASSWD_NOTREQD=0x0020
$PASSWD_CANT_CHANGE=0x0040
$NORMAL_ACCOUNT=0x0200
$DONT_EXPIRE_PASSWORD=0x10000
$PASSWORD_EXPIRED=0x800000
dim $objRootDSE
$objRootDSE=ObjGet("LDAP://rootDSE")
;~ ConsoleWrite("LDAP://"&$OU&"," & $objRootDSE.Get("defaultNamingContext"))
$objContainer = ObjGet("LDAP://"&$OU&"," & $objRootDSE.Get("defaultNamingContext"))
if $Name="" Then
   $Name=$givenName&"."&$sn
EndIf
$objNewUser = $objContainer.Create("User", "cn=" & $Name)
$objNewUser.Put( "sAMAccountName", $SamAccountName) ; login avant windows 2000
$objNewUser.Put( "userPrincipalName",$SamAccountName&"@"&getcomputeurdomain()) ; login apr�s windows 2000
$objNewUser.Put("givenName", $givenName) ; Pr�nom
if $sn="" Then
   $sn=$displayName
EndIf
$objNewUser.Put("sn", $sn) ; NOM
$objNewUser.Put("Name", $Name) ; nom aficher dans l'AD
if $displayName="" then
   $displayName=$givenName&" "&$sn
EndIf
$objNewUser.Put("displayName", $displayName) ; Nom Compl�
if not $Description="" Then
   $objNewUser.Put ("Description", $Description)
EndIf
$objNewUser.SetInfo
$objNewUser.setPassword($AccountPassword)
if $initpasswd=True Then
   $objNewUser.put ("pwdLastSet",0)
EndIf
$userAccountControl=$NORMAL_ACCOUNT
if $AccountDisabled = true Then
   $userAccountControl=$userAccountControl+$ACCOUNTDISABLE
EndIf
$objNewUser.Put("userAccountControl",$userAccountControl)
$objNewUser.SetInfo
$error=@error
If $error then
     addlignetolog ($Name&" n'a pas put �tre cr�er")

EndIf


EndFunc

func getcomputeurdomain()
$objWMISvc = ObjGet( "winmgmts:\\.\root\cimv2" )
$colItems = $objWMISvc.ExecQuery( "Select * from Win32_ComputerSystem" )
For $objItem in $colItems
    $strComputerDomain = $objItem.Domain
	Return $strComputerDomain
    If $objItem.PartOfDomain Then
        ConsoleWrite( "Computer Domain: " & $strComputerDomain)
    Else
        ConsoleWrite( "Workgroup: " & $strComputerDomain)
    EndIf
Next
EndFunc

func addlignetolog($newlogligne,$sFilePath = @DesktopDir& "\"&@ScriptName&"error.log")

   $Time=@HOUR&":"&@MIN&":"&@SEC
   if not FileExists ($sFilePath) Then
   If Not _FileCreate($sFilePath) Then Return MsgBox($MB_SYSTEMMODAL, "", "An error occurred whilst writing "&@CRLF & $sFilePath)
   EndIf
   Local $hFileOpen = FileOpen($sFilePath, $FO_APPEND)
   If $hFileOpen = -1 Then
        MsgBox($MB_SYSTEMMODAL, "", "An error occurred when reading the file.")
        Return False
   EndIf
   FileWriteLine($hFileOpen, $Time&" "&$newlogligne)
   FileClose($hFileOpen)
EndFunc

func Veille ($activation=True)
   if $activation=False Then
	  RegWrite ("HKEY_CURRENT_USER\Control Panel\Desktop","ScreenSaveActive","REG_SZ",0)
   Else
	  RegWrite ("HKEY_CURRENT_USER\Control Panel\Desktop","ScreenSaveActive","REG_SZ",1)
   EndIf
EndFunc
