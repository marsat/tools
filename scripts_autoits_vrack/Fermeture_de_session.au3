
#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.10.2
 Author:         MARSAT Guillaume

 Script Function: Fermeture de session utilisateur � la mise en veille ou par tache planifier sur un temps d'inactivit�e.

				  - forcer par gpo ou gpo local , l'utilisation de cet �crant de veille, ou planifier une tache sur inactivit� de l'ordinateur .
				  - l'utilisateur a 10 secondes avant la fermeture de session .
				  - annulation de la fermeture si on apuis sur une touche clavier ou que l'on bouge la sourie.
				  - �mission d'un bip toutes les secondes pendant le compte a rebour .
				  - modification de la taille de la barre de progression .
				  - les param�tre sont modifiable via le fichier scriptename.ini situer dans le m�me emplacement que le scripte.
				  [CONFIG]
				  timer=10
				  bipok=1
				  filewav=C:\Windows\Media\Windows Ding.wav
				  taille=3
 Script Version:  1.1
IMPORTANT: renomer le fichier.exe en fichier.scr apr�s compilation, si vous utilis�e la m�thode par l'�crant de veille.

personnelement j'utilise 2 GPO diff�rentes.
une GPO Ordinateur qui le copy les fichiers fichier.exe et fichier.ini  en local sur les postes clients a l'ouverture de l'ordinateur.
une GPO Utilisateur qui soit force les utilisateur a utiliser mon ecrant de veille , soit leur ajoute une tache planifier quant le pc et inactif.
(le 2eme cas prend en compte l'�tats global de l'ordinateur pas seulement les actions sur le clavier et la sourie, mais ne declancheras pas avans un minimum de 16 minutes
, 15minute pour valid� l'�tat d'inactivit� plus 1 minute de temps d'inactivit� avant execution).



#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#NoTrayIcon
#include <Misc.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
$fileWav=@WindowsDir&"\Media\Windows Ding.wav"
SoundSetWaveVolume(100)
$fileconf=StringSplit(@ScriptName,".")[StringSplit(@ScriptName,".")[0]-1]&".ini"
$timer=10
$bipok=1
$tailleprodress1=3
$extention=StringSplit(@ScriptName,".")[StringSplit(@ScriptName,".")[0]]
if FileExists ($fileconf) Then
$timer = IniRead ( $fileconf , "CONFIG", "timer", "10" )
$bipok =IniRead ( $fileconf , "CONFIG", "bipok", "1" )
$fileWav=IniRead ( $fileconf , "CONFIG", "filewav", @WindowsDir&"\Media\Windows Ding.wav" )
$tailleprodress1 =IniRead ( $fileconf , "CONFIG", "taille", "1" )
Else
   IniWrite($fileconf , "CONFIG", "timer", $timer)
   IniWrite($fileconf , "CONFIG", "bipok", $bipok )
   IniWrite($fileconf , "CONFIG", "filewav", $fileWav )
   IniWrite($fileconf , "CONFIG", "taille", $tailleprodress1 )
EndIf

$pidwindows=0

if $CmdLine[0] > 0 then
   if $CmdLine[1]="/s" Then
		 progressbar ()
		 Run ( "shutdown /l /f ","",@SW_HIDE)
	  ElseIf $CmdLine[1]="/c" Then
		 MsgBox ( 64,"Parrametrage","Editer le fichier de configuration "&@ScriptDir&"\"&$fileconf )
		 exit 0
	  ElseIf $CmdLine[1]="/p" Then
		 $pidwindows= $CmdLine[2]
		 If IsBinary ( StringToBinary ($pidwindows)) Then
			progressbar ()
		 EndIf
		 exit 0
	  Else
		  MsgBox ( 64,"Erreur","erreur de parrametre" )
		  Exit 10
   EndIf
Elseif $extention="au3" Then
   ConsoleWrite ( "exec from source."&@CRLF)
   progressbar ()
Elseif $extention="exe" Then
   progressbar ()
   Run ( "shutdown /l /f ","",@SW_HIDE)
EndIf
Func progressbar ()

Local $aPos = MouseGetPos()
$xorig=$aPos[0]
$yorig=$aPos[1]
$x=$xorig
$y=$yorig

Local $hTimer = TimerInit()
$roundprogres=0
if not $tailleprodress1=0 then
   $prodress1=ProgressON2 ("Fermeture de session automatique", "Fermeture de session dans:",$pidwindows,$tailleprodress1 )
EndIf
$delta=10
While ( True )
$aPos = MouseGetPos()
sleep (50)
If (($xorig+$delta) < $aPos[0]) Then

   Exit
EndIf
If (($xorig-$delta) > $aPos[0]) Then

   Exit
EndIf
If (  ($yorig+$delta)<$aPos[1]) Then

   Exit
EndIf
If (  ($yorig-$delta)>$aPos[1]) Then

   Exit
EndIf
For $i=0 to 127; on sort du programe si une touche du clavier est enfoncer.
   if _IsPressed( Hex($i,2) ) Then
	  Exit
   EndIf
Next

Local $bprogres = TimerDiff($hTimer)/1000
if not ( round($bprogres) = $roundprogres) Then
   $roundprogres=round($bprogres)
   if $bipok=1 Then
	  if FileExists ( $fileWav ) then
	  SoundPlay ( $fileWav , 0 )
	  Else
		 Beep(1300, 300)
	  EndIf
   EndIf
   if not $tailleprodress1=0 then
	  ProgressSET2 ( $prodress1,"Fermeture de session dans : "&$timer-$roundprogres&" secondes!!",($bprogres/$timer)*100)
   Endif
Else
   if not $tailleprodress1=0 then
	  ProgressSET2 ( $prodress1,"",($bprogres/$timer)*100)
   EndIf
EndIf

If ( $bprogres > $timer ) Then
   ExitLoop
EndIf
$x=$aPos[0]
$y=$aPos[1]
WEnd
EndFunc


Func ProgressON2($title,$maintext="",$parentid=0,$raport=1)
   	$left = -1
	$top = -1
	$style = BitOR($WS_SYSMENU, $WS_POPUP, $WS_POPUPWINDOW, $WS_BORDER)
	$exStyle = -1
    $width = 340
	$Height= 70
	if not $parentid=0 Then
		 $aPos = WinGetPos ( $parentid )
		 if @error Then
			Exit 2
		 EndIf
		 $raport=0.6
		 $left=($aPos[2]/2)-($width*$raport/2)
		 $top=($aPos[3]/2)- ($Height*$raport/2)
		 $exStyle = $WS_EX_MDICHILD
    EndIf
	GUICreate ( $title , $width*$raport , $Height*$raport ,$left , $top ,$style , $exStyle ,$parentid )
	GUISetFont (10*($raport), 800)
	$maintexid=GUICtrlCreateLabel ( $maintext ,5*$raport , 0,($width-10)*$raport  )
    $progressbar = GUICtrlCreateProgress((10*$raport), (20*$raport), ($width-20)*$raport, (20*$raport))
	GUISetState(@SW_SHOW)
    Return $progressbar&","&$maintexid
EndFunc

Func ProgressSET2 ( $progresOnID , $maintext="",$ProgressPoucent="")
   if not $ProgressPoucent = "" then
	  GUICtrlSetData(StringSplit ($progresOnID,",",2)[0],$ProgressPoucent )
   EndIf
      if not $maintext = "" then
	  GUICtrlSetData(StringSplit ($progresOnID,",",2)[1],$maintext)
   EndIf
EndFunc
