#!/bin/sh
## on execute le scripte dans son répertoir d'installation.
lienscript=$(readlink -f $0)
dirscript=$(dirname "$lienscript")
cd $dirscript

useragent="Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0"

fileurl="listurl"
datalist="datalist"
fallbackarchivelist="fallbackarchivelist"
normalsfile="_normals"
reflectionfile="_reflection"
echo -n "" > $normalsfile
echo -n "" > $reflectionfile

{
echo "fallback-archive=Morrowind.bsa"
echo "fallback-archive=Tribunal.bsa"
echo "fallback-archive=Bloodmoon.bsa"	
}> $fallbackarchivelist
echo  > $datalist
grep "^http" "$fileurl" | while read -r line
do
filename="${line##*/}"  
#ext="${filename##*.}"  
dirextract="${filename%.[^.]*}"
if [ ! -d $dirextract ];then
wget -U "$useragent" $line
7z x $filename -o$dirextract -y
fi

## génération des ligne data a ajouter dans openmw.cfg
{
find $dirextract -mindepth 1 -maxdepth 3 -iname "meshes" -exec dirname {} \;
find $dirextract -mindepth 1 -maxdepth 3 -iname "textures" -exec dirname {} \;
find $dirextract -mindepth 1 -maxdepth 3 -iname "*.esp" -exec dirname {} \;
find $dirextract -mindepth 1 -maxdepth 3 -iname "*.bsa" -exec dirname {} \;
} | sort -u | sed -e  "s?.*?data=\"$dirscript/&\"?" >> $datalist

{
find $dirextract -mindepth 1 -maxdepth 3 -iname "*.bsa" -exec basename {} \;
} | sed -e  "s?.*?fallback-archive=&?" >> $fallbackarchivelist

## création de la liste des fichier normal mape a renomer.
#_normal, _normals, _nrm, _nm to _n.dds
{
find $dirextract -type f -iname "*_normals.dds" -exec readlink -f {} \;
find $dirextract -type f -iname "*_normal.dds" -exec readlink -f {} \;
find $dirextract -type f -iname "*_nrm.dds" -exec readlink -f {} \;
find $dirextract -type f -iname "*_nm.dds" -exec readlink -f {} \;
} >> $normalsfile

## création de la liste des fichier reflexion a renomer.
# _reflection.dds, _s.dds to _spec.dds
{
find $dirextract -type f -iname "*_s.dds" -exec readlink -f {} \;
find $dirextract -type f  -iname "*_reflection.dds" -exec readlink -f {} \;
} >> $reflectionfile



rm -f "$filename"
done


## renomage normal maped pour openmw
cat $normalsfile | while read -r line
do
filename="$(basename "$line")"
ext="${filename##*_}"
nfile="$(echo "$line" | sed -e "s/_$ext$/_n.dds/g")"
mv "$line" "$nfile"
done

## renomage reflexion pour openmw
cat $reflectionfile | while read -r line
do
filename="$(basename "$line")"
ext="${filename##*_}"
nfile="$(echo "$line" | sed -e "s/_$ext$/_spec.dds/g")"
mv "$line" "$nfile"
done
