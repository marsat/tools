#!/bin/sh
(
tmpfile=$(mktemp)
if [ -d "$*" ] ; then
   find $* -type f | sort -r | sed -e "s/\.\///g" | grep "_s.dds$" > "$tmpfile"
   nline=$( wc -l < "$tmpfile" )
   echo "# files renaming..."
   while read -r line
   do
      n=$(echo "$line" | tr -d -c "/" | wc -c )
      if [ "$n" -ge 1 ]
      then
         dirp=$( echo "$line" | cut -d "/" -f1-"$n" )
         n=$(( n +1 ))
         file=$( echo "$line" | cut -d "/" -f$n )
         mv   "$line" "$dirp"/"$(echo "$file" | sed -e "s/_s\.dds$/_spec\.dds/g" )" 
      else
         mv  "$line" "$(echo "$line" | sed -e "s/_s\.dds$/_spec\.dds/g" )" 
      fi
      echo $(( ((i * 99 ) / nline) ))
      i=$(( i +1 ))
   done < "$tmpfile"
   
fi
if [ -f "$*" ] ; then
   n=$(echo "$*" | tr -d -c "/" | wc -c )
   if [ "$n" -ge 1 ]
   then
      dirp=$( echo "$*" | cut -d "/" -f1-"$n" )
      n=$(( n +1 ))
      file=$( echo "$*" | cut -d "/" -f$n )
      mv   "$*" "$dirp"/"$(echo "$file" | sed -e "s/_s\.dds$/_spec\.dds/g" )" 
   else
      echo 1
      mv  "$*" "$(echo "$*" | sed -e "s/_s\.dds$/_spec\.dds/g" )" 
   fi
fi
sleep 1
rm "$tmpfile"
echo "100"
 ) |
zenity --progress \
  --title="Rename _s.dds to _spec.dds < $* >" \
  --text="renaming..." \
  --percentage=0 \
  --width=400 \
  --auto-close

if [ "$?" != 0 ] ; then
   rm "$tmpfile"
   zenity --error \
          --text="Renaming cancel."

fi
