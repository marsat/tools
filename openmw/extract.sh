#!/bin/bash
## prérequit
# apt-get install zenity 7zip-full unrar
wget -c https://gitlab.com/marsat/tools/raw/master/rename_all_lower
wget -c https://gitlab.com/marsat/tools/raw/master/rename_all_space2underscore
chmod +x rename_all_lower
chmod +x rename_all_space2underscore
./rename_all_lower ./
./rename_all_space2underscore ./

find ./ -type f | grep -E "(.7z)|(.zip)" | while read -r files 
do
dir=$(echo $files | sed "s/.7z$//" | sed "s/.zip$//" | sed "s/^.\///")
7z x -o"$dir" $files

done

find ./ -type f | grep ".rar" | while read -r files 
do
dir=$(echo $files | sed "s/.rar$//" | sed "s/^.\///")
unrar x -ad $files

done

./rename_all_lower ./
./rename_all_space2underscore ./
patch=$(pwd)
basename -a $(find ./ -type f | grep ".bsa$") | sed "s?^?fallback-archive=?" > templist
find ./  -mindepth 2 -maxdepth 2 -type d | grep "data" | sed "s?^.\/?data=\"$patch/?" | sed "s/$/\"/" >> templist
find ./ -mindepth 2 -maxdepth 2 -type d | grep -E "(textures)|(icons)|(meshes)|(sound)|(fonts)" | cut -d "/" -f1-2 | sort -u | sed "s?^.\/?data=\"$patch/?" | sed "s/$/\"/" >> templist 
cat templist | sort -u > line_to_add_in_openmw.cfg
rm templist

#
