# patch
wget --header="Accept: text/html" --user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0" https://morromods.wiwiland.net/IMG/7z/PNO-2-8-3a-Manuel.7z

## rocher
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_de_la_cote_de_la_melancolie.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_de_la_cote_dazura.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_de_la_faille_de_louest.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_de_molag_amur.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_de_solstheim.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_des_grandes_patures.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_des_iles_ascadiennes.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_des_rivages.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_des_terres_cendres.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/rochers_du_mont_ecarlate.7z


## architecture
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/retexture_hlaalu_de_lougian.7z

## péisage
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/les_regions_herbeuses_de_morrowind.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/regions_herbeuses___cote_dazura_et_sheogorad.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/remplacement_des_arbres_de_longsanglot___version_2.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_de_la_region_des_terres_cendres.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_de_la_region_des_iles_ascadiennes_ii.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_de_la_region_des_grandes_patures.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_de_la_region_de_solstheim_ii.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_de_la_region_de_la_faille_de_louest_ii.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_de_la_cote_de_la_melancolie_ii.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_des_montagnes_de_moesring.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/cote_de_felsaad.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/les_montagnes_de_longsanglot.7z
## amelioration interieur
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_du_palais_du_conseil_redoran.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/les_interieurs_de_solstheim.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/les_interieurs_de_longsanglot.7z

## diverse
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/resdayn_revival___navires_et_bateaux_ameliores.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/conteneurs_animes_optimises.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/lampes_dwemers_clignotantes.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_des_modeles_de_morrowind.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/meilleurs_cranes.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/amelioration_de_larc_dos.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/armes_de_fer_ameliorees.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/armes_de_fer_uniques_ameliorees.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/armes_orientales_ameliorees.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/aspect_unique_pour_les_dagues_de_geon_auline.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/du_balai_!.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/ecritures_ameliorees.7z
## a testaer avec openmw
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/brille_dans_le_noyr.7z

## patch
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/patch_doptimisation_de_morrowind.7z

## Interface
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/barres_de_vie_des_compagnons.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/descriptions_des_benedictions.7z

## jouabilite
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/cauchemars_de_vampire_reellement_terrifiants.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/voyage_en_echassier_des_marais.7z
https://mods.confrerie-des-traducteurs.fr/morrowind/mods/voyage_en_bateau.7z

## graphyque herbalisme pour OpenMW
https://www.nexusmods.com/morrowind/mods/46599?tab=files

## core du personage
#New Beast Bodies - Clean Version by LizTail
http://mw.modhistory.com/file.php?id=10928

#Races Redone ( nouveau beter bodies )
https://www.nexusmods.com/morrowind/mods/44701?tab=files

#Better Clothes v1.1
http://mw.modhistory.com/file.php?id=14097

#More Better Clothes Vol I - Vanilla
https://www.nexusmods.com/morrowind/mods/36406?tab=files

# Hirez Better Clothes
https://www.nexusmods.com/morrowind/mods/45499?tab=files

# Robe Overhaul
https://www.nexusmods.com/morrowind/mods/43748?tab=files
# FM - Unique Items Compilation
https://www.nexusmods.com/morrowind/mods/46433
# Underwater Static Replacer v1.0
http://mw.modhistory.com/file.php?id=11998

