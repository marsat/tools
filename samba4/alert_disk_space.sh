#!/bin/bash
##déclaration des varriables
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
FILESEUIL="/usr/local/etc/alert_disk_space_seuil.conf"
defaultseuil=$(cat $FILESEUIL | grep -v "=" | grep -v "#" | grep -v "/" | grep "DEFAULT$" | awk {'print $1'})
## recupperation de la configuration mail
cat $FILESEUIL | grep "^MAILDOMAIN=" > /tmp/alert_disk_space_mail
cat $FILESEUIL | grep "^MAILTO=" >> /tmp/alert_disk_space_mail
source /tmp/alert_disk_space_mail
MAILFROM=${MAILFROM:="${HOSTNAME}@${MAILDOMAIN}"}
## on peut personaliser le seui d'allerte pour une partition en editant le fichier /usr/local/etc/alert_disk_space_seuil.conf
## déclaration des fonctions

## fonction allert retourne 1 si on depace un seuil.
allert (){
error=0
df -h | grep -v /dev/sr0 | awk {'print $5" "$6'} | sed -e "s/%//" | sed -e '1d' | while read -r ligne
do
pourcentage=$(echo $ligne | awk {'print $1'})
partition=$(echo $ligne | awk {'print $2'})
seuilpartition=$(cat $FILESEUIL | grep -v "#" | grep "$partition$" | awk {'print $1'})
if [ -z $seuilpartition ] ; then 
seuilpartition=$1
fi
if [ $pourcentage -ge $seuilpartition ];then
return 1
fi
done
error=$?
echo $error
return $error
}

##main
if [ $(allert $defaultseuil) -eq 1 ];then
## on  test si un mail d'alerte a déja était envoyer dans la journée , si oui on n en renvoi pas de second.
if [ -f /tmp/alert_disk_space_date ];then
   DATE=$(cat /tmp/alert_disk_space_date)
   if [ "$DATE" = "$(date +%Y%m%d)" ];then
#      echo "une allerte a déja était envoyer ce jour"
      exit 0
   fi
fi
echo "$(date +%Y%m%d)" > /tmp/alert_disk_space_date
{ echo "To: ${MAILTO}
From: ${MAILFROM}
Subject: alerte espace disque ${HOSTNAME}  :(

le seuil d'alerte par default ( ${defaultseuil}% ) est atteint pour au moins une partition.
ou l'un des seuil personaliser suivant a étais atteint.
cat ${FILESEUIL}
"
cat ${FILESEUIL}
echo "
df -h
"
df -h
} | msmtp ${MAILTO}
fi
