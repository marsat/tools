#!/bin/sh
# scripte de backup acl pour samba4.
# sauvegarde acl des répertoirs uniquement sur niveaux linux et windows + l'arboraicence de ces répertoirs nom_du_repertoir.acls.20180620.tar.xz
# si plus d'une sauvegarde par jours le fichier ce nomera nom_du_repertoir.acls.20180620_xx.tar.xz avec xx en
# numero de version limiter a 99 versions par jour.
# par default le backup est sauvegarder dans /root/ mais on peut passer un rep de desination en paramètre 2.
# exemple:
# backupacl.sh "/home/partagesmb/" "3" "/root/backupacl/"
dir=$(echo $1 | sed  -e "s/\/$//g")
niveaux=$2
dirbackup=$(echo -n $3 | sed  -e "s/\/$//g" | sed -e "s/ //g") 
DATE=$(date +%Y%m%d)
DATESTART=$(date -R)
if [ -z $dirbackup ] ; then
dirbackup="/root"
fi
mkdir $dirbackup
if [ ! -d $dirbackup ];then
exit 1
fi
namebackup=$(echo $dir  | sed  -e "s/^\///g" | sed  -e "s/\//_/g").acls.$DATE.tar.xz
ver=1
while [ -f "$dirbackup/$namebackup" ]
do
	ver=$(( ver + 1 ))
	namebackup=$(echo $dir  | sed  -e "s/^\///g" | sed  -e "s/\//_/g").acls.$DATE\_$ver.tar.xz
	if [ $ver -ge 99 ];then
		echo 'trops de version de backup deja créer ce jour'
		exit 2
	fi
done
echo $dirbackup/$namebackup
if [ -d $dir ];then
       rm -f  /tmp/arbo.tar.xz
       echo > /tmp/linux.facl
       echo > /tmp/linux.fattr
       find $dir -maxdepth $niveaux -type d -o -type l  | grep -v lost+found | tar -cvJf /tmp/arbo.tar.xz --no-recursion -T -

       find $dir -maxdepth $niveaux -type d -o -type l  | grep -v lost+found | getfacl -L -  > /tmp/linux.facl

       find $dir -maxdepth $niveaux -type d -o -type l  | grep -v lost+found | while read -r repertoir
       do
          getfattr -L -m - -d "$repertoir" >> /tmp/linux.fattr
       done
       tar -cvJf "$dirbackup/$namebackup" /tmp/linux.facl /tmp/linux.fattr /tmp/arbo.tar.xz
else
	echo "$1 n'est pas un répertoir"
	exit 3 
fi
echo 'vos acl on étais sauvegarder dans le fichier ci-dessous'
echo $dirbackup/$namebackup
echo "debut=$DATESTART
fin=$(date -R)"
exit 0
