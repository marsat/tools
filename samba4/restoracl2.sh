#!/bin/sh
# scripte de restoration acl pour samba4.
# restore les acl linux et windows d'un répertoir et de sont contenu a partir d'un fichier de backup généré avec backupacl.sh
# exemple:
# restoracl.sh "/root/home_partagesmb.acls.20180620_1.tar.xz" "/mysrootdir/"
# 
DATESTART=$(date -R)
filbackup=$1

if [ -f $filbackup ] ;then
	md5listfiletar=$(tar -tJf $filbackup |  md5sum | cut -d ' ' -f1) ## on recupaire la som md5 de la liste des fichier de l'archive
	if [ $md5listfiletar = "c0a9e737e8ddc458f490c57aebeea59c" ];then ## on verrifie que l'archive contien bien les bons fichiers
		tar -xJf $filbackup -C /tmp/
		cd /
		tar -xJf /tmp/arbo.tar.xz -C /
		setfacl --restore=/tmp/linux.facl
		setfattr --restore=/tmp/linux.fattr
	else
		echo "$1 n'est pas un fichier de backupacl valide."
		exit 1 
	fi
else
	echo "$1 n'est pas un fichier de backupacl valide."
	exit 1
fi
echo 'vos acl on étais restaurée'
echo "debut=$DATESTART
fin=$(date -R)"
exit 0
