#!/bin/bash
# script qui va initializer les droits sur un partage samba4  
ussage="supacl.sh \"DIR\" \"GROUP\" 
avec DIR un repertoire non systeme pour pas casser "
rep=$( echo $1 | sed  -e "s/\/$//g" )
admin_share=$2
if [ ! -d $rep ]; then
echo "supacl.sh $rep $2 ANNULER!
care $rep n'est pas un repertoir"
exit 1
fi

getent group "$admin_share" > /dev/null
errorgroup=$?
if [ $errorgroup -gt 0 ]; then
echo "supacl.sh $rep $2 ANNULER!
care $admin_share n'est pas un group valid"
exit 1
fi
if (! whiptail --title "supacl.sh" --yesno "ATTENTION vous allez initialiser donc effacer tout les droit et acls du répertoire $rep et de ces sous répertoires, voulez vous continuer" 10 60); then 
echo "supacl.sh $rep $2 ANNULER!"
exit 1
fi



if [ -d $rep ] ; then
## on liste des fichiers avec des NTACL
getfattr -R -n security.NTACL $rep 2>/dev/null | grep "^# file:" | sed -e "s/^# file: /\//g" | sed -e "s/ /\\\ /g" > /tmp/security.NTACL

## on liste des fichier avec des DOSATTRIB
getfattr -R -n user.DOSATTRIB $rep 2>/dev/null | grep "^# file:" | sed -e "s/^# file: /\//g" | sed -e "s/ /\\\ /g" > /tmp/user.DOSATTRIB 

## on liste les fichier et dossier avec des acllinux

getfacl -R -n /cdc | grep "^# file:" |sed -e "s/^# file: /\//g" | sed -e "s/ /\\\ /g"  > /tmp/user.FACL

## on suprime les FACL en récurcife
while read line
do
echo $line
setfacl -b -k "$line"
done < /tmp/user.FACL

## on supprimme tous les acl linux en récurcife
# la metode en une ligne pause des problème avec les nom de fichier et répertoir avec des espace
#setfacl -b -k -R "$rep"

## on suprime les NTACL des fichiers en possédents.
while read line
do
echo $line
setfattr -x security.NTACL "$line"
done < /tmp/security.NTACL

## on suprime les DOSATTRIB des fichiers en possédents
while read line
do
echo $line
setfattr -x user.DOSATTRIB "$line"
done < /tmp/user.DOSATTRIB

fi

## on atributs les drois au group d'administration des partage
chown -R root:"$admin_share" "$rep"
chmod -R 770 "$rep"

## on réatribut les bons droit sur les fichier lost+found des partitions.
find "$rep" -name lost+found -exec chown  root:root {} \; -exec chmod 700 {} \;


