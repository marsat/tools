#!/bin/bash
# on desactive l'ipv6
SED="/bin/sed -i"
test=`grep net.ipv6.conf.all.disable_ipv6= /etc/sysctl.conf |wc -l`
if [ $test -ge "1" ] ; then
	$SED "s?^net.ipv6.conf.all.disable_ipv6=.*?net.ipv6.conf.all.disable_ipv6=1?g" /etc/sysctl.conf
else
	echo "net.ipv6.conf.all.disable_ipv6=1" >> /etc/sysctl.conf
fi
unset test
test=`grep net.ipv6.conf.default.disable_ipv6= /etc/sysctl.conf |wc -l`
if [ $test -ge "1" ] ; then
$SED "s?^net.ipv6.conf.default.disable_ipv6=.*?net.ipv6.conf.default.disable_ipv6=1?g" /etc/sysctl.conf
else
	echo "net.ipv6.conf.default.disable_ipv6=1" >> /etc/sysctl.conf
fi
unset test
test=`grep net.ipv6.conf.lo.disable_ipv6= /etc/sysctl.conf |wc -l`
if [ $test -ge "1" ] ; then
	$SED "s?^net.ipv6.conf.lo.disable_ipv6=.*?net.ipv6.conf.lo.disable_ipv6=1?g" /etc/sysctl.conf
else
	echo "net.ipv6.conf.lo.disable_ipv6=1" >> /etc/sysctl.conf
fi
unset test
sysctl -p /etc/sysctl.conf

######################
