#!/bin/bash 
# CAlocal.sh
#
# par Guillaume MARSAT
# 
# 
#
 
CAOU="myorganisme Root Certification Authority"
CACN="CA_myorganisme"
CAC="FR"
CAST="FRANCE"
CAL="cognac"
CAO="myorganisme"
CADIR=$HOME"/CA"
CADAYSLIFE=10000 
SRVDAYSLIFE=3650
CAKEY=$CADIR/$CACN.key
CACRT=$CADIR/$CACN.crt
SIZE=2048
CACONFDIR="/usr/share/ca-certificates/CAlocal"
initCA () {
echo "initCA"
DIR_TMP=${TMPDIR-/tmp}/ctparental-mkcert.$$
mkdir $DIR_TMP
rm -fr $CADIR 2> /dev/null
rm -fr $CACONFDIR 2> /dev/null
mkdir $CADIR 2> /dev/null
chmod -R 700 $CADIR
umask 0077
mkdir $CADIR/crt 2> /dev/null
mkdir $CADIR/key 2> /dev/null


## création de la clef priver ca et du certificat ca
openssl genrsa  $SIZE > $CAKEY 
openssl req -new -x509 -subj "/C=$CAC/ST=$CAST/L=$CAL/O=$CAO/CN=$CACN/OU=$CAOU" -days $CADAYSLIFE -key $CAKEY > $CACRT 
## création des certificat pour localhost + init des compteurs de la CA.
openssl genrsa $SIZE > $CADIR/key/localhost.key 
openssl req -new -subj "/C=$CAC/ST=$CAST/L=$CAL/O=$CAO/CN=localhost" -days $CADAYSLIFE  -key $CADIR/key/localhost.key > $DIR_TMP/localhost.csr 
openssl x509 -req -in $DIR_TMP/localhost.csr -out $CADIR/crt/localhost.crt -CA $CACRT -CAkey $CAKEY -CAcreateserial -CAserial $CADIR/$CACN.srl  
umask 0022
rm -rf $DIR_TMP
echo "ajout de la CA dans les CA de confiance"
mkdir $CACONFDIR
cp -f  $CACRT $CACONFDIR
echo "fin initCA"
}

newCRT_KEY () {
umask 0077
DIR_TMP=${TMPDIR-/tmp}/ctparental-mkcert.$$
mkdir $DIR_TMP
clear
echo "Entrer le trigrame du site:"
while (true); do	
	read trigsite
	case $trigsite in
		* )		
			break
		;;
		
	esac
done
clear
echo "Entrer le CN du serveur:"
while (true); do	
	read CNserv
	case $CNserv in
		* )		
			break
		;;
		
	esac
done

openssl genrsa  $SIZE > $CADIR/key/$CNserv_$trigsite.key 
openssl req -new -subj "/C="$CAC"/ST="$CAST"/L="$CAL"/O="$CAO"/CN="$CNserv -key $CADIR/key/$CNserv.key > $DIR_TMP/$CNserv_$trigsite.csr 
openssl x509 -req -days $SRVDAYSLIFE -in $DIR_TMP/$CNserv_$trigsite.csr -out $CADIR/crt/$CNserv_$trigsite.crt -CA $CACRT -CAkey $CAKEY -CAserial $CADIR/$CACN.srl 
rm -rf $DIR_TMP
umask 0022
}




usage="Usage: CAlocal.sh    {-init } |  {-newsrv } "
case $1 in
   -\? | -h* | --h*)
      echo "$usage"
      exit 0
      ;;
   -init)
      initCA
      exit 0
      ;;
   -newsrv)
      newCRT_KEY
      exit 0
      ;;
        
   *)
      echo "Argument inconnu :$1";
      echo "$usage";
      exit 1
      ;;
esac


