#!/bin/sh
# $Id: alcasar-init_secret_pass.sh Thu, 18 May 2017 18:34:16 +0200 

# alcasar-file_ossi.sh
# by Marsat Guillaume
# This script is distributed under the Gnu General Public License (GPL)

# Script permettant 
#	- d'initialiser tous les secret et mots de passe d'Alcasar.
echo "regénération clef rsa 2 4096 bit pour sshd"
rm -rf /etc/ssh/ssh_host_*
ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ""

alcasar-profil.sh -p
passwd root
passwd sysadmin

echo "init des mots de passes utilisateur mysql root et radius ainssi que des élément secret uam et radius"
# ******* Files parameters - paramètres fichiers *********
DIR_WEB="/var/www/html"				# directory of APACHE
DIR_ACC="$DIR_WEB/acc"				# directory of the 'ALCASAR Control Center'
DIR_DEST_BIN="/usr/local/bin"			# directory of ALCASAR scripts
PASSWD_FILE="/root/ALCASAR-passwords.txt"	# text file with the passwords and shared secrets
OLDPASSWD_FILE="/root/oldALCASAR-passwords.txt"	# text file with the passwords and shared secrets
# ******* DBMS parameters - paramètres SGBD ********
DB_RADIUS="radius"				# database name used by FreeRadius server
DB_USER="radius"				# user name allows to request the users database
DB_GAMMU="gammu"				# database name used by Gammu-smsd
SED="/bin/sed -i"
if [ ! -e "$PASSWD_FILE" ] ;then
echo "le fichier ""$PASSWD_FILE"" n'est pas présent."
exit
fi
cp -f $PASSWD_FILE $OLDPASSWD_FILE
oldmysqlpwd="$(cat $OLDPASSWD_FILE | grep "root /" | cut -d " " -f 10)"
#echo ">$oldmysqlpwd<"
oldradiuspwd="$(cat $OLDPASSWD_FILE | grep "radius /" | cut -d " " -f 10)"
#echo ">$oldradiuspwd<"

# On crée aléatoirement les mots de passe et les secrets partagés
rm -f $PASSWD_FILE
grubpwd=`cat /dev/urandom | tr -dc [:alnum:] | head -c8`
echo -n "Password to protect the GRUB boot menu (!!!qwerty keyboard) : " > $PASSWD_FILE
echo "$grubpwd" >> $PASSWD_FILE
md5_grubpwd=`/usr/bin/openssl passwd -1 $grubpwd`
$SED "/^password.*/d" /boot/grub/menu.lst
$SED "1ipassword --md5 $md5_grubpwd" /boot/grub/menu.lst
mysqlpwd=`cat /dev/urandom | tr -dc [:alnum:] | head -c15`
echo -n "Name and password of Mysql/mariadb administrator : " >> $PASSWD_FILE
echo "root / $mysqlpwd" >> $PASSWD_FILE
radiuspwd=`cat /dev/urandom | tr -dc [:alnum:] | head -c14`
echo -n "Name and password of Mysql/mariadb user : " >> $PASSWD_FILE
echo "$DB_USER / $radiuspwd" >> $PASSWD_FILE
secretuam=`cat /dev/urandom | tr -dc [:alnum:] | head -c8`
echo -n "Shared secret between the script 'intercept.php' and coova-chilli : " >> $PASSWD_FILE
echo "$secretuam" >> $PASSWD_FILE
secretradius=`cat /dev/urandom | tr -dc [:alnum:] | head -c8`
echo -n "Shared secret between coova-chilli and FreeRadius : " >> $PASSWD_FILE
echo "$secretradius" >> $PASSWD_FILE
chmod 640 $PASSWD_FILE

# changement des mots de pass root 
mysqladmin -u root -p$oldmysqlpwd password $mysqlpwd

# chamgement du mots de pass mysql radius
mysql -uroot -p$mysqlpwd mysql --exec "SET PASSWORD FOR 'radius'@'localhost' = PASSWORD('$radiuspwd');"



# modification des fichier de configuration ALCASAR impactés.

$SED "s?^radiuspwd=.*?radiuspwd=\"$radiuspwd\"?g" $DIR_DEST_BIN/alcasar-mysql.sh $DIR_DEST_BIN/alcasar-conf.sh
$SED "s?\$radiuspwd = .*?\$radiuspwd = \"$radiuspwd\"\;?g" $DIR_ACC/phpsysinfo/includes/xml/portail.php
$SED "s?^sql_password:.*?sql_password: $radiuspwd?g" /etc/freeradius-web/admin.conf
$SED "s?^[\t ]*password =.*?password = \"$radiuspwd\"?g" /etc/raddb/sql.conf
$SED "s/^p_db=\".*/p_db=\"$radiuspwd\"/g" $DIR_DEST_BIN/alcasar-sms.sh

$SED "s?^\$uamsecret =.*?\$uamsecret = \"$secretuam\";?g" $DIR_WEB/intercept.php
$SED "s?^uamsecret.*?uamsecret	\"$secretuam\"?g" /etc/chilli.conf 

$SED "s?^radiussecret.*?radiussecret=\"$secretradius\"?g" $DIR_DEST_BIN/alcasar-logout.sh
$SED "s?^radiussecret.*?radiussecret	\"$secretradius\"?g" /etc/chilli.conf 
$SED "s?.*secret.*?	secret = \"$secretradius\"?g" /etc/raddb/clients.conf

service mysqld restart
service radiusd restart
service chilli restart
service httpd restart
service sshd restart



