# $Id: alcasar_let's_encrypt_phase1.sh Tue, 05 Jun 2018 21:47:55 +0200

# alcasar_let's_encrypt_phase1.sh
# by Marsat Guillaume
# This script is distributed under the Gnu General Public License (GPL)

# Script permettant 
#	- d'installer un certificat Let's Encrypt pour votre Alcasar 3.2 pour cdc avec l'API OVH.
export PATH="/usr/local/sbin:/usr/sbin:/usr/local/bin:/usr/bin:/opt/acme.sh"
cd /root/
if [ $? -ge 1 ] ;then
echo erreur
exit 10
fi
DIR_PKI=/etc/pki
DIR_CERT=$DIR_PKI/tls
DIR_WEB=/var/www/html
CACERT=$DIR_PKI/CA/alcasar-ca.crt
SRVKEY=$DIR_CERT/private/alcasar.key
SRVCERT=$DIR_CERT/certs/alcasar.crt
SRVCHAIN=$DIR_CERT/certs/server-chain.crt

DOMAIN="mydomain.fr"
ALCASAR="alcasar"
alcasardnsname=$ALCASAR.$DOMAIN

wget https://github.com/Neilpang/acme.sh/archive/master.tar.gz
tar zxvf master.tar.gz
rm master.tar.gz
cd acme.sh-master/
./acme.sh --install --nocron
cd ..
rm -rf acme.sh-master/

# https://github.com/Neilpang/acme.sh/blob/dev/dnsapi/README.md
# pour ovh => https://github.com/Neilpang/acme.sh/wiki/How-to-use-OVH-domain-api

# voir keypass
# application key
export OVH_AK="sdfsdfsfsfsfsdf"

# application secret
export OVH_AS="fssdfsfsdfsdfsfsfsfsfsfsdfsdfsdfsdf"
acme.sh --issue -d $alcasardnsname --dns dns_ovh > phase1.txt
grep "Please open this link to do authentication:" phase1.txt



