#! /bin/bash
# ajoute tous les utilisateurs sans group au group DEFAULTGROUP

PASSWD_FILE="/root/ALCASAR-passwords.txt"
DB_RADIUS="radius"
DB_USER=$(grep '^db_user=' $PASSWD_FILE | cut -d'=' -f 2-)
DB_PASS=$(grep '^db_password=' $PASSWD_FILE | cut -d'=' -f 2-)

MYSQL_USER=`/usr/bin/mysql -u$DB_USER -p$DB_PASS $DB_RADIUS -ss --execute  "SELECT username FROM radcheck ;"`
for u in $MYSQL_USER
do
   group=`/usr/bin/mysql -u$DB_USER -p$DB_PASS $DB_RADIUS -ss --execute  "SELECT groupname FROM radusergroup WHERE username = '$u'	;"`
   if [ $(echo $group | wc -m) -eq 1 ] ; then
      echo $u
      /usr/bin/mysql -u$DB_USER -p$DB_PASS $DB_RADIUS -ss --execute  "INSERT INTO radusergroup (username,groupname) VALUE ('$u','DEFAULTGROUP');"
      echo "ajout de $u dans DEFAULTGROUP"
   fi
done

