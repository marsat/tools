#!/bin/bash
# $Id: alcasar-file_ossi.sh Thu, 18 May 2017 18:34:16 +0200 

# alcasar-file_ossi.sh
# by Marsat Guillaume
# This script is distributed under the Gnu General Public License (GPL)

# Script permettant 
#	- d'importer un fichier de blacklist ossi local ou via https de l'activer , de le désactiver et de le supprimer.
#	- de lister les fichiers importés et leur état d'activation.

bl_ossi_user=""
bl_ossi_password=""
file_imp=""
link=""
wgetconection=""
CONF_FILE="/usr/local/etc/alcasar.conf"
SED="/bin/sed -i"
dir_dg="/etc/dansguardian/lists/"
dir_blacklist="$dir_dg""blacklists/"
dir_etc="/usr/local/etc/"
bl_categories="$dir_etc""alcasar-bl-categories"
bl_categories_enabled="$dir_etc""alcasar-bl-categories-enabled"
temp_dir_ossi_bl="/tmp/ossi_bl"
IPTABLES="/usr/sbin/iptables"
EXTIF=$(grep ^EXTIF= $CONF_FILE|cut -d"=" -f2)				# EXTernal InterFace
usage="Usage : alcasar-file_ossi.sh { -i ( https | http | ftp )://mysserver/myossibl.txt [ -u user -p password ]} | -i /home/sysadmin/myblacklist_ossi.txt} | {-remove} | {-remove-all} | {-disable} | {-enable} | {-list}"

remove_file_ossi_bl ()
{
	$SED "/^$1$/d" "$bl_categories_enabled"
	line=$(echo "$dir_blacklist""$1" | sed -e "s?\/?\\\/?g")
	$SED "/^$line$/d" "$bl_categories"
	$SED "/^#$line$/d" "$bl_categories"
	rm -rf "$dir_blacklist""$1"
}

imp_ossi_bl ()
{
	
	if [ ! -e "$1" ];then
		echo 'Invalid file path!'
		exit 1
	fi
	file_name=$(basename "$1" | tr "[:blank:]" "_" | tr "." "_"   )
	dest_dir="$dir_blacklist""ossi-bl-""$file_name" # /etc/dansguardian/list/blacklist/ossi-bl-XXXXXXXX
	if [ -d "$dest_dir" ];then
		exit 2
	fi	
	mkdir "$dest_dir"
	cp "$1" "$dest_dir""/domains" # copy in the file "domains" (containing @ip & domain names (like over Toulouse categories)) 
	/usr/bin/dos2unix "$dest_dir""/domains" 
	$SED "/[äâëêïîöôüû]/d" "$dest_dir""/domains" # suppression des caractères acccentués 
	$SED "/^#.*/d" "$dest_dir""/domains" # suppression des lignes commentées
	$SED "/^$/d" "$dest_dir""/domains" # suppression des lignes vides
	$SED "s/\.\{2,10\}/\./g" "$dest_dir""/domains" # suppression des suite de point consécutif.
	touch "$dest_dir""/urls"  #create the URL file even if it isn't used
	echo "$dest_dir" >> "$bl_categories" # add to the categories list
	echo "ossi-bl-""$file_name" >> "$bl_categories_enabled" # Enabled by default
}

download_ossi_bl ()
{
	if [ "$protocol" = "https" ] ; then
		$IPTABLES -A OUTPUT -o "$EXTIF" -p tcp --dport https -j ACCEPT
	fi	
	if [ "$protocol" = "ftp" ] ; then
		$IPTABLES -A OUTPUT -o "$EXTIF" -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
	fi	
	if [ ! -z "$bl_ossi_user" ] && [ ! -z "$bl_ossi_password" ];then
		wgetconection=" --user=$bl_ossi_user --password=$bl_ossi_password "
	fi

	rm -rf $temp_dir_ossi_bl
	mkdir $temp_dir_ossi_bl
	wget $wgetconection -P $temp_dir_ossi_bl $1 
	if [ ! $? -eq 0 ]; then
		echo 'error when downloading, interrupted process'
		rm -rf $temp_dir_ossi_bl
		exit 3 
	fi
	if [ "$protocol" = "https" ] ; then
		$IPTABLES -D OUTPUT -o "$EXTIF" -p tcp --dport https -j ACCEPT
	fi	
	if [ "$protocol" = "ftp" ] ; then
		$IPTABLES -D OUTPUT -o "$EXTIF" -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
	fi	
}

tabarg=($*)
narg=0
for arg in $* ; do
	narg=$(( $narg + 1 ))
	case $arg in  
		-i )    
			protocol=$( echo "${tabarg[$narg]}" | cut -d ":" -f1 )
			if [ "$protocol" = "https" ] || [ "$protocol" = "http" ] || [ "$protocol" = "ftp" ] ; then
				link=${tabarg[$narg]}
			else
				file_imp=${tabarg[$narg]}
			fi

		;;
		-u )
			bl_ossi_user=${tabarg[$narg]}
		;;
		-p )
			bl_ossi_password=${tabarg[$narg]}
		;;
		-remove )
		select ossibl in  $(basename -a "$( cat < $bl_categories | grep "ossi-bl-"  )" 2> /dev/null );	do			 
			 remove_file_ossi_bl "$ossibl"
			 /usr/local/bin/alcasar-bl.sh --reload
			 break
		done

		;;
		-remove-all )
		for ossibl in  $(basename -a "$( cat < $bl_categories | grep "ossi-bl-"  )" 2> /dev/null );	do			 
			 remove_file_ossi_bl "$ossibl"
		done
		/usr/local/bin/alcasar-bl.sh --reload
		;;
		-disable )
		select ossibl in  $( cat < $bl_categories_enabled | grep "ossi-bl-"  );	do	
			$SED "/^$ossibl$/d" "$bl_categories_enabled" 
			/usr/local/bin/alcasar-bl.sh --reload
			break
		done

		;;
		-enable )
		select ossibl in  $(basename -a "$( cat <  $bl_categories | grep "^#" | grep "ossi-bl-" )" 2>/dev/null);	do			
			if [ ! "$(cat < $bl_categories_enabled | grep -c "$ossibl" )" -ge 1 ] ; then
				echo "$ossibl" >> "$bl_categories_enabled"
				/usr/local/bin/alcasar-bl.sh --reload
			fi
			break
		done
	
		;;
		-list )
			for ossibl in  $(basename -a "$( cat < $bl_categories | grep "ossi-bl-" | grep -v "^#"  )" 2> /dev/null ); do
		                        echo "$ossibl""	enabled"
			done
			for ossibl in  $(basename -a "$( cat < $bl_categories | grep "ossi-bl-" | grep "^#"  )" 2> /dev/null ); do
		                        echo "$ossibl""	disabled"
			done
		;;	

		-help | --help | -h )
		echo "$usage";
		;;
	esac
done

if [ ! -z "$file_imp" ]  ;then
		imp_ossi_bl "$file_imp"	
		/usr/local/bin/alcasar-bl.sh --reload	
fi
if [ ! -z "${link}" ]  ;then
		download_ossi_bl "${link}"
		imp_ossi_bl "$temp_dir_ossi_bl""/$(basename "${link}")"	
		rm -rf $temp_dir_ossi_bl
		/usr/local/bin/alcasar-bl.sh --reload	
fi
	
exit 0
