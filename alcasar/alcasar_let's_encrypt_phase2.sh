# $Id: alcasar_let's_encrypt_phase2.sh Tue, 05 Jun 2018 21:47:55 +0200

# alcasar_let's_encrypt_phase2.sh
# by Marsat Guillaume
# This script is distributed under the Gnu General Public License (GPL)

# Script permettant 
#	- d'installer un certificat Let's Encrypt pour votre Alcasar 3.2. phase2
cd /root/
if [ $? -ge 1 ] ;then
echo erreur
exit 10
fi

export PATH="/usr/local/sbin:/usr/sbin:/usr/local/bin:/usr/bin:/opt/acme.sh"



DIR_PKI=/etc/pki
DIR_CERT=$DIR_PKI/tls
DIR_WEB=/var/www/html
CACERT=$DIR_PKI/CA/alcasar-ca.crt
SRVKEY=$DIR_CERT/private/alcasar.key
SRVCERT=$DIR_CERT/certs/alcasar.crt
SRVCHAIN=$DIR_CERT/certs/server-chain.crt
DOMAIN="mydomain.fr"
ALCASAR="alcasar"
alcasardnsname=$ALCASAR.$DOMAIN
DIRLETSENCRYPT="/usr/local/etc/letsencrypt/certs/$alcasardnsname/"
acme.sh --issue -d $alcasardnsname --dns dns_ovh > phase2.txt
cat phase2.txt
rm -f $CACERT
rm -f $SRVKEY
rm -f $SRVCERT
rm -f $SRVCHAIN
rm -f $DIR_WEB/certs/certificat_alcasar_ca.crt
rm -f $DIR_WEB/certs/certificat_alcasar.crt
ln -s ${DIRLETSENCRYPT}$alcasardnsname.cer $SRVCERT
ln -s ${DIRLETSENCRYPT}$alcasardnsname.key $SRVKEY
ln -s ${DIRLETSENCRYPT}ca.cer $CACERT
ln -s ${DIRLETSENCRYPT}fullchain.cer $SRVCHAIN
ln -s ${DIRLETSENCRYPT}ca.cer $DIR_WEB/certs/certificat_alcasar_ca.crt
ln -s ${DIRLETSENCRYPT}$alcasardnsname.cer $DIR_WEB/certs/certificat_alcasar.crt
systemctl reload httpd

{
echo "#!/bin/sh
export PATH=\"/usr/local/sbin:/usr/sbin:/usr/local/bin:/usr/bin:/opt/acme.sh\"
DATE=\$(date +%Y%m%d)
NbJourAvantExp=\$(( 24 * 60 * 60 * 7 ))
echo \$NbJourAvantExp
if ! openssl x509 -checkend \$NbJourAvantExp -in ${DIRLETSENCRYPT}$alcasardnsname.cer 1> /dev/null
then
{
    echo \"\$DATE renouvellement_certificat\"
    source /opt/acme.sh/acme.sh.env
    acme.sh --upgrade
    acme.sh --renew --home /opt/acme.sh/ --force -d $alcasardnsname
    systemctl reload httpd
} >> /var/log/acme.log
   
   dernierligne=\$(wc -l  /var/log/acme.log | cut -d \" \" -f1)
   ENDCERT=\"-----END CERTIFICATE-----\"
   LIGNELOGENDCERT=\$(cat  /var/log/acme.log | sed -n \"\$((dernierligne -4 ))p\")
   if [ \"\$LIGNELOGENDCERT\" = \"\$ENDCERT\" ];then
      /usr/bin/php -r \"mail('administrateur@cdcvalsdesaintonge.fr', 'update Cert Lets Encrypt Alcasar matha - RAS :)', 'le Certificat $alcasardnsname mi a jour corectement', 'cdt');\";
      echo 'ok'
   else
      /usr/bin/php -r \"mail('administrateur@cdcvalsdesaintonge.fr', 'update Cert Lets Encrypt Alcasar matha - ERROR :(', 'le Certificat $alcasardnsname na pas pus ce maitre a jour correctement', 'cdt');\";
      echo 'error'
   fi

fi
"
} > /usr/local/sbin/cron_acme.sh
chmod +x /usr/local/sbin/cron_acme.sh
{
echo '
0 3 * * * root /usr/local/sbin/cron_acme.sh
'
} > /etc/cron.d/acme.sh
systemctl reload crond
