#!/bin/bash
## scripte permétant de jouet a ark ( steam ) avec plusieurs profiles différants sur la me session d'ordinateur.
## pour l'utiliser créer un dossier $HOME/Profiles_locals_ARKet copier y le scripte.
## randre le scripte executable: chmod +x $HOME/Profiles_locals_ARK/profiles_ARK.sh
## lancer $HOME/Profiles_locals_ARK/profiles_ARK.sh -i qui va créer un profile avec la sauvegarde actuel d'ARK
## puis faire $HOME/Profiles_locals_ARK/profiles_ARK.sh --add autant de foie que vous avez besoin de profile.
## dans le lansseur arque créé par steam sur le Bureau changer la commande de lancement
## steam steam://rungameid/346110 par $HOME/Profiles_locals_ARK/profiles_ARK.sh
## en cochant la casse executer dans un terminal.
## puis lancer le jeux via le racourci ainssi modifié. 

export pid=$$
## fonctions ##
funcinputprofile () {
    returnvar=$(whiptail --title "profile" --nocancel --inputbox "Entre le nom du profile
 - Seulement des letters ou des numbers.
" 10 60 3>&1 1>&2 2>&3)
    test="$(echo "$returnvar" | grep -E "^([a-zA-Z0-9])*$")"
    if [ ! "${#test}" -ge 1  ];then
       echo "le nom de profile ne doit contenir que des carractaires alphanumeriques."
       kill -9 $pid
    fi
    echo $returnvar
}

funcinitprofile () {
if [ ! -h $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/LocalProfiles ];then
    profile=$(funcinputprofile)
    mkdir -p $HOME/Profiles_locals_ARK/$profile/DinoExports
    mkdir -p $HOME/Profiles_locals_ARK/$profile/LocalProfiles
    mkdir -p $HOME/Profiles_locals_ARK/$profile/SavedArksLocal
    mkdir -p $HOME/Profiles_locals_ARK/$profile/Config
    cp -r $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/DinoExports $HOME/Profiles_locals_ARK/$profile/
    rm -rf $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/DinoExports
    cp -r $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/Config $HOME/Profiles_locals_ARK/$profile/
    rm -rf $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/Config
    cp -r $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/LocalProfiles $HOME/Profiles_locals_ARK/$profile/
    rm -rf $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/LocalProfiles
    cp -r $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/SavedArksLocal $HOME/Profiles_locals_ARK/$profile/
    rm -rf $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/SavedArksLocal
else
    echo "Profiles_locals_ARK déja initialisé"
    kill -9 $pid
fi
}

funcselectprofile () {
    listrep=$(find $HOME/Profiles_locals_ARK/ -mindepth 1 -maxdepth 1 -type d | sed -e "s?${HOME}/Profiles_locals_ARK/??")
    listprofiles=""
    i=1
    for profile in $listrep
    do 
    if [ $i -eq 1 ];then
    listprofiles="$listprofiles $profile ON"
    else
    listprofiles="$listprofiles $profile OFF"
    fi
    i=2
    done
    returnvar=$(whiptail --nocancel --backtitle "ARK" --title "Selection du Profile ARK" --radiolist --noitem "Veuillez selectionner votre profile" 14 40 6 ${listprofiles[@]} 3>&1 1>&2 2>&3 )
    echo $returnvar
}
profileexiste () {
    test=0
    profileatester="$1"
    listrep=$(find $HOME/Profiles_locals_ARK/ -mindepth 1 -maxdepth 1 -type d | sed -e "s?${HOME}/Profiles_locals_ARK/??")
    for profile in $listrep
    do
        if [ "$profile" = "$profileatester" ];then
            
            echo 1
            exit 0
        fi
    done
    echo 0
}

startprofile () {
profileselect=$(funcselectprofile)

rm -f $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/DinoExports
rm -f $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/Config
rm -f $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/LocalProfiles
rm -f $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/SavedArksLocal
ln -s $HOME/Profiles_locals_ARK/$profileselect/DinoExports $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/DinoExports
ln -s $HOME/Profiles_locals_ARK/$profileselect/Config $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/Config
ln -s $HOME/Profiles_locals_ARK/$profileselect/LocalProfiles $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/LocalProfiles
ln -s $HOME/Profiles_locals_ARK/$profileselect/SavedArksLocal $HOME/.steam/steam/steamapps/common/ARK/ShooterGame/Saved/SavedArksLocal

sync
{
echo '#!/bin/bash
steam steam://rungameid/346110
'
} > $HOME/Profiles_locals_ARK/ARK
chmod 755 $HOME/Profiles_locals_ARK/ARK

nohup $HOME/Profiles_locals_ARK/ARK && kill -9 $pid 

}
## main ##
if [ ! -d $HOME/Profiles_locals_ARK ];then
initprofile
fi

usage=" profiles_ARK.sh [ -a | --add ] || [ -i | --install ] || [ -r | --remove ] || [ -h | -help | --help ] 
 sans aucun parramètre lance ARK sur un profile choisi."

case $* in  
        -i | --install )
            funcinitprofile
        ;;
        -a | --add )
            newprofile=$(funcinputprofile)
            echo $newprofile
            while [ $(profileexiste "$newprofile" ) -eq 1 ]
            do
                echo "prifile déja existant veiller changer de nom de profile." 
                sleep 2
                newprofile=$(funcinputprofile)
            done
            echo "Le profile de $newprofile est ajouter."
            mkdir -p $HOME/Profiles_locals_ARK/$newprofile/DinoExports
            mkdir -p $HOME/Profiles_locals_ARK/$newprofile/Config
            mkdir -p $HOME/Profiles_locals_ARK/$newprofile/LocalProfiles
            mkdir -p $HOME/Profiles_locals_ARK/$newprofile/SavedArksLocal
        ;;
        -r | --remove )
            removedprofile=$(funcselectprofile)
            whiptail --yesno --backtitle "ARK" --title "Suppression du Profile ARK" "Voulez vous vraiment supprimer le profile  $removedprofile" 14 40 6
            if [ $? -eq 0 ];then
            rm -rf $HOME/Profiles_locals_ARK/$removedprofile/
            echo "Le Profile $removedprofile a été supprimer."
            fi
        ;;
        -help | --help | -h )
            echo "$usage";
        ;;
        * )
            startprofile
        ;;
esac


