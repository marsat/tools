#!/bin/sh
#scripte permétent de générer un fichier de conf unbound basée sur la blackliste de l'universitée de toulouse + celle de ctparental
SED="/bin/sed -i"
#DIR_CONF="$(dirname "$(readlink -f "$0")")"
DIR_CONF="/usr/local/etc/blunbound"
SAFE_CONF="$DIR_CONF/CTsafe.conf"
FILE_CONF="$DIR_CONF/CTparental.conf"
unboundzonefile="/etc/unbound/unbound.conf.d/unbound.nopub.conf"
mv -f ${unboundzonefile} ${unboundzonefile}.$(date +%Y%m%d_%M_%S)
mkdir -p $DIR_CONF

cat << EOF > $unboundzonefile
server:
EOF

if [ ! -f $FILE_CONF ] ; then
cat << EOF > $FILE_CONF
MD5DREAB=0
EOF
fi
if [ ! -f $SAFE_CONF ] ; then
cat << EOF > $SAFE_CONF
#SAFEGOOGLE
#SAFEYOUTUBE
EOF
fi
tempDIR="$DIR_CONF/tmp/alcasar"
tempDIRRamfs="$DIR_CONF"
FILE_tmp=${FILE_tmp:="$tempDIRRamfs/filetmp.txt"}
if [ ! -d $tempDIRRamfs ]; then mkdir "${tempDIRRamfs}"; fi
BL_SERVER="dsi.ut-capitole.fr"
CATEGORIES_ENABLED="$DIR_CONF/categories-enabled.conf"
BL_CATEGORIES_AVAILABLE="$DIR_CONF/bl-categories-available"
WL_CATEGORIES_AVAILABLE="$DIR_CONF/wl-categories-available"
DIR_DNS_FILTER_AVAILABLE="$DIR_CONF/dnsfilter-available"
DIR_DNS_BLACKLIST_ENABLED="$DIR_CONF/blacklist-enabled"
DIR_DNS_WHITELIST_ENABLED="$DIR_CONF/whitelist-enabled"
DNS_FILTER_OSSI="$DIR_CONF/blacklist-local.conf"
DREAB="$DIR_CONF/domaine-rehabiliter.conf" 
if [ ! -f $DNS_FILTER_OSSI ] ; then
	echo > $DNS_FILTER_OSSI
fi
if [ ! -f $DREAB ] ; then
	echo > $DREAB
fi
if [ ! -f $CATEGORIES_ENABLED ] ; then
cat << EOF > $CATEGORIES_ENABLED
adult
agressif
ctparental
dangerous_material
cryptojacking
dating
drogue
gambling
hacking
malware
marketingware
mixed_adult
phishing
redirector
publicite
ddos
sect
strict_redirector
strong_redirector
tricheur
warez
ossi 
EOF
fi
download() {
   rm -rf $tempDIR
   mkdir -p $tempDIR
   # on attend que la connection remonte suite au redemarage de networkmanager
   gettext 'Waiting to connect to Toulouse server:'
   i=1
   while [ "$(ping -c 1 $BL_SERVER 2> /dev/null | grep -c "1 received"  )" -eq 0 ]
   do
   echo -n .
   sleep 1
   i=$(( i + 1 ))
   if [ $i -ge 40 ];then # si au bout de 40 secondes on a toujours pas de connection on considaire qu'il y a une erreur
		gettext 'Connection to Toulouse server is impossible.'
		set -e
		exit 1
   fi
   done
   echo
   gettext 'connection established:'
   
   wget -P $tempDIR http://$BL_SERVER/blacklists/download/blacklists.tar.gz 2>&1 | cat
   if [ ! $? -eq 0 ]; then
      gettext 'error when downloading, interrupted process'
      rm -rf $tempDIR
      set -e
      exit 1
   fi
   tar -xzf $tempDIR/blacklists.tar.gz -C $tempDIR
   if [ ! $? -eq 0 ]; then
      gettext 'archive extraction error, interrupted process'
      set -e
      exit 1
   fi
   # on télécharge aussi la blackliste personalisé de ctparental
   wget -P $tempDIR https://gitlab.com/marsat/bl_ctparental/raw/master/blacklist.tar.gz 2>&1 | cat
   if [ ! $? -eq 0 ]; then
      gettext 'error when downloading, interrupted process'
      rm -rf $tempDIR
      set -e
      exit 1
   fi
   tar -xzf $tempDIR/blacklist.tar.gz -C $tempDIR
   if [ ! $? -eq 0 ]; then
      gettext 'archive extraction error , interrupted process'
      set -e
      exit 1
   fi
   rm -rf ${DIR_DNS_FILTER_AVAILABLE:?}/
   mkdir $DIR_DNS_FILTER_AVAILABLE
   $SED "s?^MD5DREAB.*?MD5DREAB=?g" $FILE_CONF
}

netoyage_domain_name() {
$SED -r '/([0-9]{1,3}\.){3}[0-9]{1,3}/d' "$1" # on supprime les IP
$SED "/[äâëêïîöôüû_]/d" "$1"  # on supprime les ligne contenet des carractaire spéciaux.
$SED "/^#.*/d" "$1" 		  # on suprime les lignes de commantaire
$SED "/^$/d" "$1" 			  # on suprime les lignes vide
$SED "/^-/d" "$1"			  # on suprime les domaine commancent par un - exemple: -big-tits.com et supprimer
$SED '/-./d' "$1"			  # on suprime les domaine ayant un point juste drerrier un - exemple: fucking-big-tits-.com et supprimer
$SED "s/\.\{2,10\}/\./g" "$1" # supprime les suite de "." exemple: fucking-big-tits..com devient fucking-big-tits.com
$SED "s/ //g" "$1"			  # on suprime tous les espaces.
}

adapt_available() {
echo "adapt_available"
date +%H:%M:%S


if [ ! -f $DNS_FILTER_OSSI ] ; then
	echo > $DNS_FILTER_OSSI
fi
if [ -d $tempDIR  ] ; then
	CATEGORIES_AVAILABLE="$tempDIR"/categories_available
	echo -n > $CATEGORIES_AVAILABLE
	echo -n > $WL_CATEGORIES_AVAILABLE
	echo -n > $BL_CATEGORIES_AVAILABLE
	if [ ! -f $DIR_DNS_FILTER_AVAILABLE/ossi.conf ] ; then
		echo > $DIR_DNS_FILTER_AVAILABLE/ossi.conf
	fi
	gettext 'Toulouse Blacklist and WhiteList migration process. Please wait.'
	cd "$tempDIR"/blacklists
	find . -type l -exec rm -f {} \; # suppessiondes liens symbolique
	for categorie in *
	do
		if [ -d "$categorie" ] ; then
			if [ ! -L "$categorie" ] ; then 
				echo "$categorie" >> $CATEGORIES_AVAILABLE
				echo -n "."
				cp -f "$tempDIR"/blacklists/"$categorie"/domains "$FILE_tmp"
				if [ $categorie = "adult" ] ; then
				$SED "/.*blogspot\..*/d" "$FILE_tmp" # on suprime les 1313541 entrée xx.blogspot.xx dans la categorie adult qui son gérai par une liste beaucoup plus courte dans la catégorie ctparental
				fi
				netoyage_domain_name "$FILE_tmp"
				if [ -e "$tempDIR"/blacklists/"$categorie"/usage ] ; then
					if [ "$(grep -c "white" "$tempDIR"/blacklists/"$categorie"/usage)" -ge 1 ] ;then
						echo "$categorie" >> $WL_CATEGORIES_AVAILABLE
						mv "$FILE_tmp" "$DIR_DNS_FILTER_AVAILABLE"/"$categorie".conf
					else
						echo "$categorie" >> $BL_CATEGORIES_AVAILABLE
						mv "$FILE_tmp" "$DIR_DNS_FILTER_AVAILABLE"/"$categorie".conf  	
					fi				
				else
					echo "$categorie" >> $BL_CATEGORIES_AVAILABLE
					mv "$FILE_tmp" "$DIR_DNS_FILTER_AVAILABLE"/"$categorie".conf  	
				fi
			fi
		fi
	done
	echo -n "."
	# suppression des @IP, de caractères acccentués et des lignes commentées ou vides
	cp -f $DNS_FILTER_OSSI "$FILE_tmp"
	netoyage_domain_name "$FILE_tmp"
	mv "$FILE_tmp" "$DIR_DNS_FILTER_AVAILABLE"/ossi.conf

else
	mkdir   $tempDIR
	echo -n "."
	# suppression des @IP, de caractères acccentués et des lignes commentées ou vides
	cp -f $DNS_FILTER_OSSI "$FILE_tmp"
	netoyage_domain_name "$FILE_tmp"
	mv "$FILE_tmp" "$DIR_DNS_FILTER_AVAILABLE"/ossi.conf
fi     
echo
cd "$(dirname "$(readlink -f "$0")")"

date +%H:%M:%S
}

catChoice() {
echo "<catChoice>"
md5old=$(grep MD5DREAB= "$FILE_CONF" | cut -d"=" -f2)
md5new="$({
cat "$DREAB" "$CATEGORIES_ENABLED"
md5sum $DIR_DNS_FILTER_AVAILABLE/* | grep -v ossi.conf
} | md5sum )"
## évite de lancer la moulinette de réabilitation des domaines quant aucun changements
## ne le nécécite.
if [ ! "$md5old" = "$md5new" ]; then
	rm -rf ${DIR_DNS_BLACKLIST_ENABLED:?}/
	mkdir $DIR_DNS_BLACKLIST_ENABLED
	rm -rf  ${DIR_DNS_WHITELIST_ENABLED:?}/
	mkdir  $DIR_DNS_WHITELIST_ENABLED  
	while read CATEGORIE
	do
		if [ "$(grep -c "$CATEGORIE" "$BL_CATEGORIES_AVAILABLE")" -ge "1" ] ; then
			cp $DIR_DNS_FILTER_AVAILABLE/"$CATEGORIE".conf $DIR_DNS_BLACKLIST_ENABLED/
		else
			cp $DIR_DNS_FILTER_AVAILABLE/"$CATEGORIE".conf $DIR_DNS_WHITELIST_ENABLED/
		fi     
	done < $CATEGORIES_ENABLED
	reabdomaine
	$SED "s?^MD5DREAB.*?MD5DREAB=$md5new?g" $FILE_CONF

fi



cp $DIR_DNS_FILTER_AVAILABLE/ossi.conf $DIR_DNS_BLACKLIST_ENABLED/
echo "</catChoice>"
}

reabdomaine () {
echo "<reabdomaine>"
date +%H:%M:%S

if [ ! -f $DREAB ] ; then
cat << EOF > $DREAB
EOF
fi
netoyage_domain_name "$DREAB"
if [ ! -f $DIR_DNS_BLACKLIST_ENABLED/ossi.conf ] ; then
	echo > $DIR_DNS_BLACKLIST_ENABLED/ossi.conf
fi
echo
gettext 'Application whitelisting (restored area):'
while read CATEGORIE
do 
	if [ "$(grep -c "$CATEGORIE" "$BL_CATEGORIES_AVAILABLE" )" -ge "1" ] ; then
		echo -n "."
		while read DOMAINE
		do
		    cp -f $DIR_DNS_BLACKLIST_ENABLED/"$CATEGORIE".conf "$FILE_tmp"
		    $SED "/$DOMAINE/d" "$FILE_tmp"
            cp -f "$FILE_tmp" $DIR_DNS_BLACKLIST_ENABLED/"$CATEGORIE".conf
		done < $DREAB		
    fi
done < $CATEGORIES_ENABLED




date +%H:%M:%S

}

adapt_enabled () {
cd $DIR_DNS_BLACKLIST_ENABLED/
cat * | sed -e "s/^\.\{1,10\}//g" | sort -u > "$FILE_tmp"
cd $(dirname "$(readlink -f "$0")")
$SED "s?.*?local-zone: \"&\" redirect \nlocal-data: \"& A 127.0.0.10\"?g" "$FILE_tmp" # mise en forme unbound
cat  "$FILE_tmp" >> $unboundzonefile 
{
## on force a passer par forcesafesearch.google.com de maninière transparente
forcesafesearchgoogle=$(host -ta forcesafesearch.google.com|cut -d" " -f4)	# retrieve forcesafesearch.google.com ip
echo -n > "$FILE_tmp"
if [ "$(cat < $SAFE_CONF | grep -c "^SAFEGOOGLE" )" -eq 1 ];then
	echo "# forcesafesearch redirect server for google" 
	for subdomaingoogle in $(wget http://www.google.com/supported_domains -O - 2> /dev/null )  # pour chaque sous domain de google
	do 
	echo "www$subdomaingoogle" 	
	done
fi

if [ "$(cat < $SAFE_CONF | grep -c "^SAFEYOUTUBE" )" -eq 1 ];then
	echo "www.youtube.com"
fi
} > $DIR_DNS_BLACKLIST_ENABLED/google.conf
cd $DIR_DNS_BLACKLIST_ENABLED/
cat google.conf  | sed -e "s/^\.\{1,10\}//g" | sort -u > "$FILE_tmp"
cd $(dirname "$(readlink -f "$0")")
$SED "s?.*?local-zone: \"&\" redirect \nlocal-data: \"& A $forcesafesearchgoogle\"?g" "$FILE_tmp" # mise en forme unbound
cat  "$FILE_tmp" >> $unboundzonefile 
}


echo "</reabdomaine>"

usage="Use: update_bl_unbound.sh    { -dl } }
-dl	    => updates parental control from the blacklist of the University of Toulouse)
"

arg1=${1}
case $arg1 in
	-\? | -h* | --h*)
		echo "$usage"
	;;
	
	-dl )
		download
		adapt_available
		catChoice
		adapt_enabled
	;;
	*)
	echo "Unknown argument:$1";
	echo "$usage";
	exit 1
	;;
esac

