#!/bin/bash

file_list_vncserver=$(grep "file_list_vncserver=" /usr/local/sbin/novnc.sh | cut -d"=" -f2 | sed -e "s/\"//g" )
if [ ! -f $file_list_vncserver ];then
    echo "le fichier $file_list_vncserver existe pas."
    exit 1
fi


grep "^[1-9]" $file_list_vncserver | while read -r line
do
num=$(echo $line | cut -d":" -f1 )
systemctl restart novnc@$num.service
done

