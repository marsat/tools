#!/bin/bash
file_list_vncserver="/usr/local/etc/novnc.csv"

if [ $(grep -c -E "^[0-9]{1,3}$" <<< ${1}) -ne 1 ];then
   echo "syntax error"
   exit 1
fi
num=${1}


line=$(grep  "^${num}:" $file_list_vncserver)
if [ -z "$line" ];then
   echo "serveur vnc non présente dans le fichier de configuration"
   exit 2
fi
#echo $line
webdir=$( echo $line | awk -F ":" '{print $6}')
certfile=$( echo $line | awk -F ":" '{print $7}')
lhost=$( echo $line | awk -F ":" '{print $8}')
webdir=${webdir:="/usr/share/novnc/"}
certfile=${certfile:="/root/novnc.pem"}
lhost=${lhost:="localhost"}
lport=$( echo $line | awk -F ":" '{print $2}')
hostsrvvnc=$( echo $line | awk -F ":" '{print $3}')
portsrvvnc=$( echo $line | awk -F ":" '{print $4}')

/usr/bin/websockify  --web=$webdir --cert=$certfile $lhost:$lport $hostsrvvnc:$portsrvvnc

exit
