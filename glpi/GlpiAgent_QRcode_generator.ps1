Import-Module QRCodeGenerator

#qrcode pour agent Glpi
#le principe et le suivent c'est un QRcode text d'une chaine Base64 UTF8 corespondent au text formater comme ci-dessous
#{"URL":"","TAG":"","LOGIN":"","PASSWORD":""}
#
#eyJVUkwiOiIiLCJUQUciOiIiLCJMT0dJTiI6IiIsIlBBU1NXT1JEIjoiIn0=
#

## compléter les variables avec les valeurs a rentrer dans l'agent
$url='https://srv-glpi:443/front/inventory.php'
$tag='myTag'
$login='mylogin'
$password='mypassword'



$readableText = '{"URL":"'+$url+'","TAG":"'+$tag+'","LOGIN":"'+$login+'","PASSWORD":"'+$password+'"}"'
$encodedBytes = [System.Text.Encoding]::UTF8.GetBytes($readableText)
$encodedText = [System.Convert]::ToBase64String($encodedBytes)
$encodedText
$OutPath="$env:USERPROFILE\GlpiAgentQRcode.png"
New-PSOneQRCodeText -text $encodedText -OutPath $OutPath -Width 10 -Show