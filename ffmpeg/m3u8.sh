#!/bin/bash
# apt install ffmpeg bc curl
if [ ! -f "$1" ];then
	listfile=$(ls -1 *.m3u8)
	select cexpfile in ${listfile} exit ; 
	do 
	   case $cexpfile in
		  exit) echo "exiting"
				exit 0
				break ;;
			 *) echo "file select is $cexpfile";
				break ;;
	   esac
	done
	date="$( date +%d.%m.%Y.%H.%M.%S)"
	if [ ! -f $cexpfile ];then
	   exit 0 
	fi
else
cexpfile="${1}"
date="$(echo ${1} | sed -e "s/\.m3u8//g")"
fi
if [ "${2}" != "" ];then
startdlseg=${2}
else
startdlseg=1
fi

origin=${origin:="https://mydomain.lan"}
referer=${referer:="https://mydomain.lan/"}
echo $cexpfile
unset videoname
purge=${purge:="0"}
videoname=${videoname:=M3U8.$date}
mkdir "$videoname"


if [ ! -f "$videoname/unencripted.ts" ] ; then

	keyurl=$(grep 'URI="' $cexpfile  | sed -e "s/,/\n/g" | grep "^URI=" | sed -e "s/URI=//g" | sed -e "s/\"//g")
	IVkey=$(grep 'URI="' $cexpfile  | sed -e "s/,/\n/g" | grep "^IV=" | sed -e "s/IV=//g" | sed -e "s/\"//g")
	IVkey=${IVkey:=1}
	echo $keyurl
	echo $origin
	if [ -n "$keyurl" ]; then
	   if [ $startdlseg = 1  ];then
		 curl "$keyurl" \
		 -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:131.0) Gecko/20100101 Firefox/131.0' \
		 -H 'Accept: */*' \
		 -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' \
		 -H 'Accept-Encoding: gzip, deflate, br, zstd' \
		 -H "Origin: $origin" \
		 -H 'Connection: keep-alive' \
		 -H "Referer: $referer" \
		 -H 'Sec-Fetch-Dest: empty' \
		 -H 'Sec-Fetch-Mode: cors' \
		 -H 'Sec-Fetch-Site: cross-site' > ${videoname}/encryption.key
	   fi
	   hexkey=$(xxd -p ${videoname}/encryption.key)
	fi
	hostname=${hostname:="my.domain.lan"}
	starturl=$(grep 'starturl=' $cexpfile | cut -d"=" -f2)
	cp $cexpfile temp.m3u8
	sed -i '/^#EXT-X-KEY/d' temp.m3u8
	segts=1
	grep '.ts' $cexpfile | awk '{ print $1 }' | while read -r url
	do
	    dest=${videoname}/unencript$(printf "%03d\n" ${segts}).ts
	    sed -i 's#'"${url}"'#'"${dest}"'#g' temp.m3u8
		segts=$((segts+1))	
	done
	
	segts=1
	grep '.ts' $cexpfile | awk '{ print $1 }' | while read -r url
	do 
		if [ "$(echo $url | grep -c "^http")" = "0" ];then
			url="$starturl$url"
		fi
		echo "$url"
		echo $origin
		if [ $segts -ge $startdlseg ];then
		echo $segts
		   whait=$(echo "scale=2; $(( $RANDOM % 1000))/400 " | bc)
		   sleep $whait
		   curl "$url" \
			  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:131.0) Gecko/20100101 Firefox/131.0' \
			  -H 'Accept: */*' \
			  -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' \
			  -H 'Accept-Encoding: gzip, deflate, br, zstd' \
			  -H "Origin: $origin" \
			  -H 'Connection: keep-alive' \
			  -H "Referer: $referer" \
			  -H 'Sec-Fetch-Dest: empty' \
			  -H 'Sec-Fetch-Mode: cors' \
			  -H 'Sec-Fetch-Site: cross-site' > ${videoname}/encript$(printf "%03d\n" ${segts}).ts
		fi
		if [ -n "$keyurl" ]; then
		    openssl enc  -aes-128-cbc -d -K $hexkey -iv $IVkey -in ${videoname}/encript$(printf "%03d\n" ${segts}).ts -out ${videoname}/unencript$(printf "%03d\n" ${segts}).ts
		    rm ${videoname}/encript$(printf "%03d\n" ${segts}).ts
		
		else
			mv ${videoname}/encript$(printf "%03d\n" ${segts}).ts ${videoname}/unencript$(printf "%03d\n" ${segts}).ts
		fi
	segts=$((segts+1))	
	done
	cat ${videoname}/unencript*.ts > ${videoname}/unencripted.ts
	cd ${videoname}


	echo $hexkey


	if [ "$purge" = "1" ];then
	   find ./ -regextype sed -regex "\.\/[0-9]\{3\}.ts" | while read -r file
	   do
		  rm -f $file
	   done
	   rm -f encripted.ts
	fi
else
   cd ${videoname}
fi
countpistvideo=$(ffmpeg -i unencripted.ts  2>&1 | grep -c "Stream #0.*Video")
countpistaudio=$(ffmpeg -i unencripted.ts  2>&1 | grep -c "Stream #0.*Audio")
error="250"
if [ "$countpistvideo" != "0" ]; then
   if [ "$countpistaudio" != "0" ]; then
      # ts audio + video
      ffmpeg -i "unencripted.ts" -c:v libsvtav1 -preset 8 -crf 35 -svtav1-params fast-decode=1:tune=0  -c:a libopus -b:a 64k "../va_$videoname.mkv"
      error=$?   
   else
      # video sans audio
      ffmpeg -i "unencripted.ts" -c:v libsvtav1 -preset 8 -crf 35 -svtav1-params fast-decode=1:tune=0  "../v_$videoname.mkv" 
      error=$?
   fi
   
else
   if [ "$countpistaudio" != "0"  ]; then
      # audio sans video
      ffmpeg -i "unencripted.ts" -acodec libopus -b:a 64k "../a_$videoname.opus"
      error=$?   
   fi
fi
echo $error >> error.txt
if [ "$purge" = "1" ];then
   if [ "$error" = "0" ];then
      rm -f unencripted.ts
   fi
fi

cd ..
#rm -rf "$videoname/"
#rm -rf $cexpfile



