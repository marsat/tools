#!/bin/bash

file_list_cam=$(grep "file_list_cam=" /usr/local/sbin/streamnew.sh | cut -d"=" -f2 | sed -e "s/\"//g" )
if [ ! -f $file_list_cam ];then
    echo "le fichier $file_list_cam existe pas."
    exit 0
fi


grep "^cam" $file_list_cam | while read -r line
do
numcam=$(echo $line | cut -d":" -f1 | sed -e "s/cam//g")
systemctl restart streamcamera@$numcam.service
done
