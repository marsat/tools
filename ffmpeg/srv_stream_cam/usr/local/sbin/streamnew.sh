#!/bin/bash
file_list_cam="/etc/stream/cameras.csv"
# fichier regroupent les info de clé pour ffmpeg .
key_info_file="/etc/stream/key_info"
outputdir="/var/www/html"

if [ $(grep -c -E "^[0-9]{1,3}$" <<< ${1}) -ne 1 ];then
   echo "syntax error"
fi
numcam=${1}
if [ -n ${2} ];then
if [ "$2" = "stop" ];then
     ## on tente la supression des fichier temps que systemd n'a pas fermé le processus ffmpeg associer a la camméra
     while [ $(find  /var/www/html/cam${numcam}.S* | wc -l) -ge 1 ]
     do
     sleep 0.2
         rm -f /var/www/html/cam${numcam}.S.m3u8
         rm -f /var/www/html/cam${numcam}.S*.ts
     done
     exit 0
fi
fi

line=$(grep  "^cam${numcam}:" $file_list_cam)
if [ -z $line ];then
   echo "camera non présente dans le fichier de configuration"
fi
#echo $line
login=$( echo $line | awk -F ":" '{print $4}')
password=$( echo $line | awk -F ":" '{print $5}')
ip=$( echo $line | awk -F ":" '{print $2}')
port=$( echo $line | awk -F ":" '{print $3}')
filename=$( echo $line | awk -F ":" '{print $1}')
subtype=$( echo $line | awk -F ":" '{print $6}')


cam="rtsp://$login:$password@$ip:$port/cam/realmonitor?channel=1&subtype=$subtype"
ffmpeg  -rtsp_transport tcp  -i $cam -c:v copy -c:a copy -hls_key_info_file $key_info_file -hls_time 1 -hls_list_size 5 -hls_flags delete_segments -start_number 1 $outputdir/$filename.S.m3u8 


exit

