<?php
$numcam = filter_input(INPUT_GET, 'numcam', FILTER_SANITIZE_NUMBER_INT);
if (!isset($numcam)) {
    header('WWW-Authenticate: Basic realm=StreamRealm');
    header('HTTP/1.0 401 Unauthorized');
    exit;

} 
$cam_file="/etc/stream/cameras.csv";
$vhost="stream.my.domain.lan";

if (is_file ($cam_file))
{
    $tab = file($cam_file);

    if ($tab)
    {
        foreach ($tab as $line)
        {
            $field = explode(":", $line);
            if ($field[0] == "cam".$numcam)         { $keycamfile      = trim($field[8]); }
        }
    }
}
else
{
    echo gettext('Error opening the file')." ".$cam_file;
}

$key = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_ENCODED);
if (!isset($key)) {
    header('WWW-Authenticate: Basic realm=StreamRealm');
    header('HTTP/1.0 401 Unauthorized');
    exit;

} else {
   if ($key == $keycamfile) {
?>
<html>
  <head>
    <title>Hls.js demo - basic usage</title>
  </head>

  <body>
    <script src="dist/hls.js"></script>

    <center>
      <video height="100%" width="100%" id="video" controls></video>
    </center>

    <script>
      var video = document.getElementById('video');
      if (Hls.isSupported()) {
        var hls = new Hls({
          debug: true,
        });
<?php
echo  "        hls.loadSource('https://".$vhost."/cam".$numcam.".S.m3u8');
";
?>
        hls.attachMedia(video);
        hls.on(Hls.Events.MEDIA_ATTACHED, function () {
          video.muted = true;
          video.play();
        });
      }
      // hls.js is not supported on platforms that do not have Media Source Extensions (MSE) enabled.
      // When the browser has built-in HLS support (check using canPlayType), we can provide an HLS manifest (i.e. .m3u8 URL) directly to the video element through the s>
      // This is using the built-in support of the plain video element, without using hls.js.
      else if (video.canPlayType('application/vnd.apple.mpegurl')) {
<?php
echo  "        video.src = 'https://".$vhost."/cam".$numcam.".S.m3u8';
";
?>
        video.addEventListener('canplay', function () {
          video.play();
        });
      }
    </script>
</html>

<?php
   }
   else {
       header('WWW-Authenticate: Basic realm=StreamRealm');
       header('HTTP/1.0 401 Unauthorized');
       exit;

   }
}

?>
