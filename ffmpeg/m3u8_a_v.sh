#bin/bash
# apt install ffmpeg bc curl brotli gzip
date="$( date +%d.%m.%Y.%H.%M.%S)"
listfile=$(ls -1 *.txt)
select cexpfile in ${listfile} exit ; 
do 
   case $cexpfile in
      exit) echo "exiting"
            exit 0
            break ;;
         *) echo "file select is $cexpfile";
            break ;;
   esac
done
if [ ! -f $cexpfile ];then
   exit 0
fi

videoname=${videoname:=console-export.$date}

mkdir -p "${videoname}/nodownload/"
purge=${purge:="1"}
nfile=1

export origin=${origin:="https://mydomain.lan"}
export referer=${referer:="https://mydomain.lan/"}
echo $origin
grep '.m3u8' $cexpfile  | awk '{ print $1 }' | 	sort -u | while read -r url
do
    curl "$url" \
     -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:131.0) Gecko/20100101 Firefox/131.0' \
     -H 'Accept: */*' \
     -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' \
     -H 'Accept-Encoding: gzip, deflate, br, zstd' \
     -H "Origin: $origin" \
     -H 'Connection: keep-alive' \
     -H "Referer: $referer" \
     -H 'Sec-Fetch-Dest: empty' \
     -H 'Sec-Fetch-Mode: cors' \
     -H 'Sec-Fetch-Site: cross-site' \
     -H 'Priority: u=0' > ${videoname}/$nfile.m3u8.gz
	sleep 1
	compressed=0
	#test si gzip file
	gzip -t ${videoname}/$nfile.m3u8.gz
	gziperror=$?
	if [ "$gziperror" = "0" ];then
	    cat ${videoname}/$nfile.m3u8.gz | gunzip > ${videoname}/$nfile.m3u8
	    mv -f ${videoname}/$nfile.m3u8.gz ${videoname}/nodownload/
	    compressed=1
	fi
	#test si brotli file
	brotli -t ${videoname}/$nfile.m3u8.gz
	brotlierror=$?
	if [ "$brotlierror" = "0" ];then
		brotli -d ${videoname}/$nfile.m3u8.gz -o ${videoname}/$nfile.m3u8
	    mv -f ${videoname}/$nfile.m3u8.gz ${videoname}/nodownload/
	    compressed=1
	fi
	# si non comprésser ou compression inconue
	if [ "$compressed" = "0" ];then
		cp -f ${videoname}/$nfile.m3u8.gz ${videoname}/$nfile.m3u8
	    mv -f ${videoname}/$nfile.m3u8.gz ${videoname}/nodownload/
	fi
	
	
	sleep 1
	# le fichier doit contenir au moins 10 segment ts
	if [ "$(grep -c '.ts' ${videoname}/$nfile.m3u8)" -le "10" ];then
	    echo $(grep -c '.ts' ${videoname}/$nfile.m3u8)
	    
	   mv  -f ${videoname}/$nfile.m3u8  ${videoname}/nodownload/
	else
	   filename=$(basename $url | cut -d"?" -f1)
	   hostname="$(echo $url | cut -d"/" -f3)"
       starturl="$(echo $url | sed "s/\/$filename.*//g")/"
       echo "starturl=$starturl" >> ${videoname}/$nfile.m3u8
    fi
	nfile=$((nfile+1))

done

sleep 2
cd $videoname

###
reslist=$(cat nodownload/*.m3u8 2>/dev/null | grep "RESOLUTION=" | sed -e "s/.*RESOLUTION=/RESOLUTION=/g" | cut -d "," -f1)
if [ "$reslist" != "" ];then
	select resolution in ${reslist} all ; 
	do 
	   case $resolution in
		   all) echo "resolution select is $resolution" ;
				break ;;
			 *) echo "resolution select is $resolution";
				break ;;
	   esac
	done
	if [ "$resolution" != "all" ];then 
		selecturl=$(( $(cat nodownload/*.m3u8 | grep -n "$resolution" | cut -d":" -f1) + 1 ))
		urlm3u8selected="$(cat nodownload/*.m3u8 | sed -n "${selecturl}p")"
		filenameselected=$(basename $urlm3u8selected | cut -d"?" -f1)
		starturlselected="$(echo $urlm3u8selected | sed "s/\/$filenameselected.*//g")/"

	fi
fi
####
echo $starturlselected

cp /usr/local/bin/m3u8.sh ./
echo > pidliste
if [ $(cat *.m3u8 | grep -c "$starturlselected" ) -eq 0 ];then
   curl "$urlm3u8selected" \
     -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:131.0) Gecko/20100101 Firefox/131.0' \
     -H 'Accept: */*' \
     -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' \
     -H 'Accept-Encoding: gzip, deflate, br, zstd' \
     -H "Origin: $origin" \
     -H 'Connection: keep-alive' \
     -H "Referer: $referer" \
     -H 'Sec-Fetch-Dest: empty' \
     -H 'Sec-Fetch-Mode: cors' \
     -H 'Sec-Fetch-Site: cross-site' \
     -H 'Priority: u=0' > select.m3u8.gz
   error=$?
   if [ "$error" != "0" ];then
      echo "$selecturl non téléchargeable"
      exit 0
   fi
    compressed=0
    #test si gzip file
   	gzip -t select.m3u8.gz
	gziperror=$?
	if [ "$gziperror" = "0" ];then
	    cat select.m3u8.gz | gunzip > select.m3u8
	    mv -f select.m3u8.gz nodownload/
	    compressed=1
	fi
	#test si brotli file
	brotli -t select.m3u8.gz
	brotlierror=$?
	if [ "$brotlierror" = "0" ];then
		brotli -d select.m3u8.gz -o select.m3u8
	    mv -f select.m3u8.gz nodownload/
	    compressed=1
	fi
    # si non comprésser ou compression inconue
	if [ "$compressed" = "0" ];then
	    cp -f select.m3u8.gz select.m3u8
	    mv -f select.m3u8.gz nodownload/
	fi

fi


ls -1 *.m3u8 | while read -r file
do
echo $file
grep "$starturlselected" "$file"
if [ $(grep -c "$starturlselected" $file)  -gt 0 ];then
  ./m3u8.sh $file &
  mypid=$(ps -o pid= -p "$!")
  echo $mypid >> pidliste
fi

done

# on attet la fin des processus lancé
cat pidliste | while read -r pid
do 
   if [ "$pid" != "" ]; then
   while [ "$(ps -A | awk '{print $1}' | grep -c $pid )" != "0" ];do
     sleep 5
   done
   fi
done
###########
erro="250"
fileaudio=$(ls a_M3U8.*.opus 2>/dev/null)
filevideo=$(ls v_M3U8.*.mkv 2>/dev/null)
filevideo2=$(ls va_M3U8.*.mkv 2>/dev/null)
fileaudio=${fileaudio:="a_M3U8.notfind.opus"}
filevideo=${filevideo:="v_M3U8.notfind.mkv"}
filevideo2=${filevideo2:="va_M3U8.notfind.mkv"}
if [ -f $fileaudio ];then
ffmpeg -i "$filevideo" -i "$fileaudio" -map 0 -map 1:a -c:v copy -shortest -metadata:s:a:1 language=fra -metadata:s:a:1 title="FRA" "../$videoname.mkv"
error=$?  
else 
	if [ -f "$filevideo2" ];then
	   cp $filevideo2 ../$videoname.mkv
	   error=$?
	else
	   error="1"
	fi
fi

if [ "$purge" = "1" ];then
   if [ "$error" = "0" ];then
      cd ../
      rm -rf $videoname
      rm -f $cexpfile
   fi
fi
