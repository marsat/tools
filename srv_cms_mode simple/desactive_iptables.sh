i_WAN_ipv4=enp0s3
i_WAN_ipv6=enp0s3
ip6tables -F
ip6tables -X
ip6tables -t nat -F
ip6tables -t mangle -F    
ip6tables -t nat -X  
ip6tables -P INPUT ACCEPT
ip6tables -P OUTPUT ACCEPT
ip6tables -P FORWARD ACCEPT 
iptables -F
iptables -X
iptables -t nat -F 
iptables -t mangle -F   
iptables -t nat -X  
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT

netfilter-persistent save

