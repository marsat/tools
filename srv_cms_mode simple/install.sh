
## modifier l'interface et l'es info réseau avant de lancer ##
{
echo "
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug enp0s3
#iface enp0s3 inet dhcp
iface enp0s3 inet static
address 192.168.1.8
netmask 255.255.255.0
gateway 192.168.1.1
"
} >  /etc/network/interfaces


apt-get install ntp vim
apt-get install iptables-persistent
apt-get install ssh openssh-server
SFTP_GROUP_NAME=sftp_chroot


#prompte rouge pour root

echo "PS1='\${debian_chroot:+(\$debian_chroot)}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\# '" >> /root/.bashrc

apt-get install apache2
apt-get install mysql-client mysql-server
apt-get install php
apt-get install phpmyadmin
## configuration mysql
{
echo "
USE mysql; 
SELECT plugin FROM user WHERE user='root';
UPDATE user SET plugin='' WHERE User='root';
FLUSH PRIVILEGES;
EXIT;
"
} > unix_socket_root.sql
mysql -u root -p < unix_socket_root.sql

echo "repondre Y a tout"
mysql_secure_installation  

#tar -cvzf etc_phpmyadmin_orig.tar.gz /etc/phpmyadmin/
#### securisation phpmyadmin ####
cp htaccess.conf /etc/apache2/conf-available/
cp .htaccess /usr/share/phpmyadmin/
sed -i "s?^Alias.*?Alias /mysite /usr/share/phpmyadmin?g" /etc/apache2/conf-available/phpmyadmin.conf

### confsshd et chroot sftp users ###
sed -i "s?^X11Forwarding.*?X11Forwarding no?g"			  /etc/ssh/sshd_config
sed -i "s?^#Port.*?Port 64022?g"			  			  /etc/ssh/sshd_config
{
echo "# ajouter à la fin du fichier :
Match Group sftp_chroot
        ChrootDirectory /home/%u
        ForceCommand internal-sftp
        AllowTcpForwarding no"
} >> /etc/ssh/sshd_config
#ssh -p 64022 -L 3228:127.0.0.1:80 sysadmin@192.168.1.8

### apache en mode production ###
sed -i "s?^ServerTokens.*?ServerTokens Prod?g"			  /etc/apache2/conf-available/security.conf
sed -i "s?^ServerSignature.*?ServerSignature Off?g"		  /etc/apache2/conf-available/security.conf
sed -i "s?^expose_php.*?expose_php = Off?g"	 	  		  /etc/php/7.0/apache2/php.ini

## configuration PHP pour les besoins cms made simple
sed -i "s?^upload_max_filesize.*?upload_max_filesize = 60M?g"			  			  /etc/php/7.0/apache2/php.ini
sed -i "s?^max_execution_time.*?max_execution_time = 120?g"				  			  /etc/php/7.0/apache2/php.ini
sed -i "s?^post_max_size.*?post_max_size = 61M?g"				  			  /etc/php/7.0/apache2/php.ini






