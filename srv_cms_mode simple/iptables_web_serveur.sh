ip6tables -F
ip6tables -X
ip6tables -t nat -F
ip6tables -t mangle -F    
ip6tables -t nat -X  
ip6tables -P INPUT DROP
ip6tables -P OUTPUT DROP
ip6tables -P FORWARD DROP 
iptables -F
iptables -X
iptables -t nat -F 
iptables -t mangle -F   
iptables -t nat -X  
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP
## On drop les scans XMAS et NULL.
iptables -A INPUT -p tcp --tcp-flags FIN,URG,PSH FIN,URG,PSH -j DROP
iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
## Dropper silencieusement tous les paquets broadcastés.
iptables -A INPUT -m pkttype --pkt-type broadcast -j DROP
# TCP Syn Flood
iptables -A INPUT -p tcp --syn -m limit --limit 3/s -j ACCEPT
# UDP Syn Flood
iptables -A INPUT -p udp -m limit --limit 10/s -j ACCEPT
### IP indésirables 

### ACCEPT ALL interface loopback ###
ip6tables -A INPUT  -i lo -j ACCEPT
ip6tables -A OUTPUT -o lo -j ACCEPT
iptables -A INPUT  -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT
### accepte en entrée les connexions entrantes déjà établies (en gros cela permet d'accepter 
### les connexions initiées par sont propre PC)
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
### accepte en entrée les connexions sortante déjà établies (en gros cela permet d'accepter 
### les connexions initiées par sont propre PC)
iptables -A OUTPUT -m state ! --state INVALID -j ACCEPT
ip6tables -A OUTPUT -m state ! --state INVALID -j ACCEPT
### DHCP
#iptables -A OUTPUT -p udp --sport 68 --dport 67 -j ACCEPT
#iptables -A INPUT -p udp --sport 67 --dport 68 -j ACCEPT
#ip6tables -A OUTPUT -p udp --sport 68 --dport 67 -j ACCEPT
#ip6tables -A INPUT -p udp --sport 67 --dport 68 -j ACCEPT

### DNS indispensable pour naviguer facilement sur le web ###
iptables -A OUTPUT -p tcp -m tcp --dport 53 -j ACCEPT
iptables -A OUTPUT -p udp -m udp --dport 53 -j ACCEPT
ip6tables -A OUTPUT -p tcp -m tcp --dport 53 -j ACCEPT
ip6tables -A OUTPUT -p udp -m udp --dport 53 -j ACCEPT


### HTTP navigation internet non sécurisée ###
iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT
ip6tables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT
    
### HTTPS pour le site des banques .... ###
iptables -A OUTPUT -p tcp -m tcp --dport 443 -j ACCEPT
ip6tables -A OUTPUT -p tcp -m tcp --dport 443 -j ACCEPT

### SRV HTTP navigation internet non sécurisée ###
iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
ip6tables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
    
### SRV SSH pour le site des banques .... ###
#iptables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
#ip6tables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 64022 -j ACCEPT
ip6tables -A INPUT -p tcp -m tcp --dport 64022 -j ACCEPT

### SRV HTTPS pour le site des banques .... ###
iptables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
ip6tables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
    
### ping ... autorise à "pinger" un ordinateur distant ###
#iptables -A OUTPUT -p icmp -j ACCEPT
#ip6tables -A OUTPUT -p icmp -j ACCEPT
 
### clientNTP ... syncro à un serveur de temps ###
iptables -A OUTPUT -p udp -m udp --dport 123 -j ACCEPT
ip6tables -A OUTPUT -p udp -m udp --dport 123 -j ACCEPT

### envoit de mail ###
iptables -A OUTPUT -p tcp -m tcp --dport 465 -j ACCEPT     # smtp/ssl
ip6tables -A OUTPUT -p tcp -m tcp --dport 465 -j ACCEPT     # smtp/ssl

### LOG ### Log tout ce qui qui n'est pas accepté par une règles précédente
ip6tables -A OUTPUT -j LOG  --log-prefix "iptables6: "
ip6tables -A INPUT -j LOG   --log-prefix "iptables6: "
ip6tables -A FORWARD -j LOG  --log-prefix "iptables6: "
iptables -A OUTPUT -j LOG  --log-prefix "iptables: "
iptables -A INPUT -j LOG   --log-prefix "iptables: "
iptables -A FORWARD -j LOG  --log-prefix "iptables: "

netfilter-persistent save

{
echo '
:msg, startswith, "iptables: " /var/log/iptables/iptables.log
& ~
'
} > /etc/rsyslog.d/iptables.conf
service rsyslogd restart

{
echo '
/var/log/iptables/*.log {
        daily
        missingok
        rotate 14
        compress
        delaycompress
        notifempty
        create 640 root adm
        sharedscripts
        postrotate
		service rsyslog restart ; sleep 5
        endscript
}
'
} > /etc/logrotate.d/iptables

