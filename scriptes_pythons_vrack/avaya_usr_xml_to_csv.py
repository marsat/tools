#!/usr/bin/python3

# scripte permétent de générer un fichier .csv de l'export utilisateur Avaya qui est au format .xml
# import des libréries
from xml.etree import ElementTree
import os
import csv
import sys
from tkinter import filedialog
from tkinter import *



if len(sys.argv)<2:
   root = Tk()
   root.filename =  filedialog.askopenfilename(title = "Select file",filetypes = (("xml files","*.xml"),("all files","*.*")))
   users_avaya_xml = str(root.filename)
   dirpatch = str(os.path.dirname(root.filename))
else:
	users_avaya_xml=sys.argv[1]
    print (firstarg)
# définition du répertoire de travail et des variables 
os.chdir(os.path.dirname(root.filename))
users_avaya_csv = 'users_Avaya.csv'


# on lit le fichier xml
tree = ElementTree.parse( users_avaya_xml )

# on ouvre le fichier en ecriture et on defini un objet ecriture pour le csv
ob_users_avaya_csv = open( users_avaya_csv , 'w' , newline='' , encoding= 'utf-8')
cvswriter = csv.writer(ob_users_avaya_csv)

# on ecrit la première ligne du .csv 
cvswriter.writerow(['FullName','Extension','SIPContact','LoginCode','VoicemailEmail','VoicemailCode'])

# on recupère la racine de l'xml
root = tree.getroot()

# on créer un objet users lier a la structure de l'xml générer par Avaya
users=root.findall('data/ws_object/User')

# pour chaque utilisateur on récupère les atributs qui nous intéraise.
for data in users : 
    #print(data.tag, data.attrib)
    FullName = str(data.find('FullName').text)
    if FullName != "None" :
       Extension = str(data.find('Extension').text)
       SIPContact = str(data.find('SIPContact').text)
       LoginCode = str(data.find('LoginCode').text)
       VoicemailEmail = str(data.find('VoicemailEmail').text)
       VoicemailCode = str(data.find('VoicemailCode').text)
       line = FullName + "," + Extension + "," + SIPContact + "," + LoginCode + "," + VoicemailEmail + "," + VoicemailCode 
       print (line)
       cvswriter.writerow([FullName,Extension,SIPContact,LoginCode,VoicemailEmail,VoicemailCode])
    
# on sauvegarde les changement du fichier .csv
ob_users_avaya_csv.close()

