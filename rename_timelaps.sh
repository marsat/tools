#!/bin/sh
## on execute le scripte dans son répertoir d'installation.
lienscript=$(readlink -f $0)
dirscript=$(dirname "$lienscript")
cd $dirscript

i=1
ls -1 *.png | while read -r filename
do

echo $filename
name="${filename%.[^.]*}"
ext="${filename##*.}"
conteur=$(printf '%05d\n' $i)
mv -T "$filename" "img$conteur.$ext"
echo "$name"
echo "$ext"
i=$((i+1))
done
