﻿## script créé par Guillaume Marsat
## automatise la fermeture des ports sur des switch HP/aruba
## il peut facilement ètre adapter pour lancer une suite de commandes sur des switchs hp/aruba
## mais doit surement aussi fonctionner avec sur cisco ou tous autre conection ssh

# Install-Module -Name Posh-SSH -Force
Import-Module -Name Posh-SSH 


## !!!! passer la valeur test a $false pour une execution sur tous les switche si non seul le 192.168.0.10 sera impactée !!!!
$test=$true



# remplacer "disable" par "enable" pour réactiver les ports
$enabledisable="disable"
$wshell = New-Object -ComObject Wscript.Shell
$reponse = $wshell.Popup("Répondre Oui pour Désactiver les ports et Non pour les Réactivé",0,"Voulez vous désactiver les port de vos switchs",4)
if($reponse -eq 7){$enabledisable="enable"}

#$switchsecurestring = ""
#$User = "admin"
#$secpasswd = ConvertTo-SecureString $switchsecurestring
#$Credentials = New-Object System.Management.Automation.PSCredential($User, $secpasswd )

$Credentials = Get-Credential -credential manager

$portssh="22"


## liste des ip des switche et leurs ports a étaindre on ne coup pas les lien entre les switch seulement les ports en mode acces.
## port disable 1-8 veut dir de 1 a 8 on peut aussi l'écrire 1,2,3,4,5,6,7,8
## on desactive pas le 226 qui cerpour le PC crise (le cable le relient a notre réseaux devant ètre déconécter manuellement).
## $dataswitch devent ètre mis a jour en fonction des changement de configuration du réseaux.
$dataswitch = @(
[pscustomobject]@{ip='192.168.0.20';portdisable='1-8'}
[pscustomobject]@{ip='192.168.0.21';portdisable='1-24'}
[pscustomobject]@{ip='192.168.0.23';portdisable='1-2,4-28'}
)

if ( $test -eq $true ) {
    $dataswitch = @(
    [pscustomobject]@{ip='192.168.0.10';portdisable='1-8'}
    )
}

$script:buff=""

## fonction qui permé d'attendre un prompte après l'envoit d'une commande
function whaitprompt ($patern,$maxwhaitcommand) {
    $script:buff=""
    $startDate = Get-Date
    $endtimewhile = $startDate.Addseconds($maxwhaitcommand) 
    while (  $endtimewhile -le $(Get-Date) ) {
        sleep 0.5
        $script:buff=$script:buff+$script:objshellssh.read()
        $paternok=  $($script:buff | Select-String -Pattern $patern )
        if ($paternok.Length -ge 1 ) { 
            return $true        
            break
        }
    }
    # si on dépace le temps de récupération du prompte on ce déconnecte de la session ssh 
    Remove-SshSession -SessionId $script:SessionSSH.SessionId
    return $false
}

## pour chaques switch du $dataswitch
foreach ( $switch in $dataswitch )
{
    # on récupère l'ip et les port a fermer/ouvir
    $ip=$switch.ip
    $portdisable=$switch.portdisable
    # ouverture de la session ssh
    $script:SessionSSH = New-SshSession -ComputerName "$ip" -Credential $Credentials  -Port $portssh
    # ouverture du terminal
    $script:objshellssh = New-SSHShellStream -SessionId $script:SessionSSH.SessionId -Rows 300
    # on attent que la conexion soit établie
    whaitprompt( "Press any key to continue" , "20" )
    # on apuis sur une touche
    $script:objshellssh.WriteLine(" ")
    whaitprompt ( "# " , 10 )   
    ## on lance les commandes .
    $script:objshellssh.WriteLine("configure")
    whaitprompt ( "# " , 10 )
    $script:objshellssh.WriteLine("interface $portdisable")
    whaitprompt ( "# " , 10 )
    $script:objshellssh.WriteLine($enabledisable)
    whaitprompt ( "# " , 10 )
    $script:objshellssh.WriteLine("write memory")
    whaitprompt ( "# " , 10 )
    $script:buff

    # on ferme la sesssion ssh
    Remove-SshSession -SessionId $script:SessionSSH.SessionId
}

